<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tiposervicio".
 *
 * @property string $ID
 * @property string $tipo
 *
 * @property Servicio[] $servicios
 */
class Tiposervicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiposervicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['tipo'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicios()
    {
        return $this->hasMany(Servicio::className(), ['FK_tipoServicio' => 'ID']);
    }
}
