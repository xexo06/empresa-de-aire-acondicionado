<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Garantia;

/**
 * GarantiaSearch represents the model behind the search form of `app\models\Garantia`.
 */
class GarantiaSearch extends Garantia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nGarantia'], 'integer'],
            [['fechaFin', 'estado', 'FK_equipo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Garantia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'nGarantia' => $this->nGarantia,
            'fechaFin' => $this->fechaFin,
        ]);

        $query->andFilterWhere(['like', 'estado', $this->estado])
            ->andFilterWhere(['like', 'FK_equipo', $this->FK_equipo]);

        return $dataProvider;
    }
}
