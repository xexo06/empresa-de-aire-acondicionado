<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedor".
 *
 * @property string $RUT
 * @property string $direccion
 * @property string $nombre
 *
 * @property Producto[] $productos
 */
class Proveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RUT', 'direccion', 'nombre'], 'required'],
            [['RUT'], 'string', 'max' => 10],
            [['direccion', 'nombre', 'correo', 'telefono'], 'string', 'max' => 30],
            [['RUT'], 'unique'],
            [['RUT'], 'validateRut'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'RUT' => 'Rut',
            'direccion' => 'Dirección',
            'nombre' => 'Nombre',
            'correo' => 'Correo',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Producto::className(), ['FK_proveedor' => 'RUT']);
    }

    public function validateRut($attribute, $params) {
        $data = explode('-', $this->RUT);
        $evaluate = strrev($data[0]);
        $multiply = 2;
        $store = 0;
        for ($i = 0; $i < strlen($evaluate); $i++) {
            $store += $evaluate[$i] * $multiply;
            $multiply++;
            if ($multiply > 7)
                $multiply = 2;
        }
        isset($data[1]) ? $verifyCode = strtolower($data[1]) : $verifyCode = '';
        $result = 11 - ($store % 11);
        if ($result == 10)
            $result = 'k';
        if ($result == 11)
            $result = 0;
        if ($verifyCode != $result)
            $this->addError('RUT', 'Rut inválido.');
    }
}
