<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trabajar".
 *
 * @property string $ID
 * @property string $FK_tecnico
 * @property string $FK_servicio
 *
 * @property Tecnico $fKTecnico
 * @property Servicio $fKServicio
 */
class Trabajar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trabajar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_tecnico', 'FK_servicio'], 'required'],
            [['FK_servicio'], 'integer'],
            [['FK_tecnico'], 'string', 'max' => 10],
            [['FK_tecnico'], 'exist', 'skipOnError' => true, 'targetClass' => Tecnico::className(), 'targetAttribute' => ['FK_tecnico' => 'RUT']],
            [['FK_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['FK_servicio' => 'nFactura']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FK_tecnico' => 'Fk Tecnico',
            'FK_servicio' => 'Fk Servicio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKTecnico()
    {
        return $this->hasOne(Tecnico::className(), ['RUT' => 'FK_tecnico']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKServicio()
    {
        return $this->hasOne(Servicio::className(), ['nFactura' => 'FK_servicio']);
    }
}
