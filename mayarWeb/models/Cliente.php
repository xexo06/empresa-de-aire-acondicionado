<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property string $RUT
 * @property string $correo
 * @property string $ciudad
 * @property string $nombre
 * @property string $telefono
 * @property string $direccion
 *
 * @property Equipo[] $equipos
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RUT', 'correo', 'ciudad', 'nombre', 'telefono', 'direccion'], 'required'],
            [['RUT'], 'string', 'max' => 10],
            [['correo', 'ciudad', 'nombre', 'telefono', 'direccion'], 'string', 'max' => 30],
            [['RUT'], 'unique',],
            [['RUT'], 'validateRut'],
            ['correo', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'RUT' => 'Rut',
            'correo' => 'Correo',
            'ciudad' => 'Ciudad',
            'nombre' => 'Nombre',
            'telefono' => 'Teléfono',
            'direccion' => 'Dirección',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasMany(Equipo::className(), ['FK_cliente' => 'RUT']);
    }

    public function validateRut($attribute, $params) {
        $data = explode('-', $this->RUT);
        $evaluate = strrev($data[0]);
        $multiply = 2;
        $store = 0;
        for ($i = 0; $i < strlen($evaluate); $i++) {
            $store += $evaluate[$i] * $multiply;
            $multiply++;
            if ($multiply > 7)
                $multiply = 2;
        }
        isset($data[1]) ? $verifyCode = strtolower($data[1]) : $verifyCode = '';
        $result = 11 - ($store % 11);
        if ($result == 10)
            $result = 'k';
        if ($result == 11)
            $result = 0;
        if ($verifyCode != $result)
            $this->addError('RUT', 'Rut inválido.');
    }
}
