<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tener".
 *
 * @property string $ID
 * @property string $FK_servicio
 * @property string $FK_equipo
 *
 * @property Servicio $fKServicio
 * @property Equipo $fKEquipo
 */
class Tener extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tener';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FK_servicio', 'FK_equipo'], 'required'],
            [['FK_servicio'], 'integer'],
            [['FK_equipo'], 'string', 'max' => 30],
            [['FK_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['FK_servicio' => 'nFactura']],
            [['FK_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipo::className(), 'targetAttribute' => ['FK_equipo' => 'nSerie']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FK_servicio' => 'Fk Servicio',
            'FK_equipo' => 'Fk Equipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKServicio()
    {
        return $this->hasOne(Servicio::className(), ['nFactura' => 'FK_servicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKEquipo()
    {
        return $this->hasOne(Equipo::className(), ['nSerie' => 'FK_equipo']);
    }
}
