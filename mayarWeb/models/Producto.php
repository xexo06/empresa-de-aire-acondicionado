<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property string $ID
 * @property int $stock
 * @property string $refrigerante
 * @property string $modelo
 * @property string $tipo
 * @property string $FK_proveedor
 *
 * @property Equipo[] $equipos
 * @property Proveedor $fKProveedor
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stock', 'refrigerante', 'modelo', 'tipo', 'FK_proveedor','capacidad'], 'required'],
            [['capacidad'], 'integer'],
            [['stock'], 'integer'],
            [['refrigerante', 'modelo', 'tipo'], 'string', 'max' => 30],
            [['FK_proveedor'], 'string', 'max' => 10],
            [['FK_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedor::className(), 'targetAttribute' => ['FK_proveedor' => 'RUT']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'stock' => 'Stock',
            'refrigerante' => 'Refrigerante',
            'modelo' => 'Marca',
            'tipo' => 'Tipo',
            'FK_proveedor' => 'RUT Proveedor',
            'capacidad' => 'Capacidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasMany(Equipo::className(), ['FK_producto' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKProveedor()
    {
        return $this->hasOne(Proveedor::className(), ['RUT' => 'FK_proveedor']);
    }
}
