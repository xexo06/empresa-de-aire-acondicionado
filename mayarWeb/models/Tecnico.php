<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tecnico".
 *
 * @property string $RUT
 * @property string $correo
 * @property string $telefono
 * @property string $direccion
 * @property string $nombre
 *
 * @property Trabajar[] $trabajars
 */
class Tecnico extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tecnico';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RUT', 'correo', 'telefono', 'direccion', 'nombre'], 'required'],
            [['RUT'], 'string', 'max' => 10],
            [['correo', 'telefono', 'direccion', 'nombre'], 'string', 'max' => 30],
            [['RUT'], 'unique'],
            [['RUT'], 'validateRut'],
            ['correo', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'RUT' => 'Rut',
            'correo' => 'Correo',
            'telefono' => 'Teléfono',
            'direccion' => 'Dirección',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajars()
    {
        return $this->hasMany(Trabajar::className(), ['FK_tecnico' => 'RUT']);
    }

    public function validateRut($attribute, $params) {
        $data = explode('-', $this->RUT);
        $evaluate = strrev($data[0]);
        $multiply = 2;
        $store = 0;
        for ($i = 0; $i < strlen($evaluate); $i++) {
            $store += $evaluate[$i] * $multiply;
            $multiply++;
            if ($multiply > 7)
                $multiply = 2;
        }
        isset($data[1]) ? $verifyCode = strtolower($data[1]) : $verifyCode = '';
        $result = 11 - ($store % 11);
        if ($result == 10)
            $result = 'k';
        if ($result == 11)
            $result = 0;
        if ($verifyCode != $result)
            $this->addError('RUT', 'Rut inválido.');
    }
}
