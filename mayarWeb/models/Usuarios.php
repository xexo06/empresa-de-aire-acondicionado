<?php

namespace app\models;

use Yii;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $Id
 * @property string $Rut
 * @property string $First_name
 * @property string $Last_name
 * @property string $Telefono
 * @property string $Username
 * @property string $Email
 * @property string $Password
 * @property string $authKey
 * @property string $User_level
 */
class Usuarios extends ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Rut', 'First_name', 'Last_name', 'Telefono', 'Username', 'Email', 'Password', 'authKey'], 'required'],
            [['User_level'], 'string'],
            [['Rut', 'Telefono'], 'string', 'max' => 30],
            [['First_name', 'Last_name', 'Username', 'Password', 'authKey'], 'string', 'max' => 250],
            [['Email'], 'string', 'max' => 500],
            [['Rut'], 'validateRut'],
            ['Email', 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'Rut' => 'Rut',
            'First_name' => 'Nombre',
            'Last_name' => 'Apellido',
            'Telefono' => 'Teléfono',
            'Username' => 'Username',
            'Email' => 'Email',
            'Password' => 'Password',
            'authKey' => 'Auth Key',
            'User_level' => 'User Level',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function isUserAdministrador($username){
        if(static::findOne(['Username' => $username, 'User_level'=>'Administrador'])){
            return true;
        }else{
            return false;
        }
    }


    public static function isUserSecretaria($username){
        if(static::findOne(['Username' => $username, 'User_level'=>'Secretaria'])){
            return true;
        }else{
            return false;
        }
    }


    public static function findIdentity($id){
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null){
        throw new NotSupportedException();//I don't implement this method because I don't have any access token column in my database
    }

    public function getId(){
        return $this->Id;
    }

    public function getAuthKey(){
        return $this->authKey;//Here I return a value of my authKey column
    }

    public function validateAuthKey($authKey){
        return $this->authKey === $authKey;
    }

    public static function findByUsername($username){
        return self::findOne(['Username'=>$username]);
    }

    public function validatePassword($password){
        return Yii::$app->security->validatePassword($password, $this->Password);
    }
    public function validateRut($attribute, $params) {
        $data = explode('-', $this->Rut);
        $evaluate = strrev($data[0]);
        $multiply = 2;
        $store = 0;
        for ($i = 0; $i < strlen($evaluate); $i++) {
            $store += $evaluate[$i] * $multiply;
            $multiply++;
            if ($multiply > 7)
                $multiply = 2;
        }
        isset($data[1]) ? $verifyCode = strtolower($data[1]) : $verifyCode = '';
        $result = 11 - ($store % 11);
        if ($result == 10)
            $result = 'k';
        if ($result == 11)
            $result = 0;
        if ($verifyCode != $result)
            $this->addError('RUT', 'Rut inválido.');
    }
}
