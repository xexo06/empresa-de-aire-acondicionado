<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pago".
 *
 * @property string $ID
 * @property int $numeroCuotas
 * @property int $valorServicio
 * @property int $valorEquipo
 * @property int $interes
 * @property string $FK_servicio
 *
 * @property Cuotas[] $cuotas
 * @property Servicio $fKServicio
 */
class Pago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['valorServicio', 'valorEquipo', 'interes', 'FK_servicio'], 'required'],
            [['valorServicio', 'valorEquipo', 'interes', 'FK_servicio'], 'integer'],
            [['FK_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['FK_servicio' => 'nFactura']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'valorServicio' => 'Valor Servicio',
            'valorEquipo' => 'Valor Equipo',
            'interes' => 'Interés',
            'FK_servicio' => 'Factura',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuotas()
    {
        return $this->hasMany(Cuotas::className(), ['FK_pago' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKServicio()
    {
        return $this->hasOne(Servicio::className(), ['nFactura' => 'FK_servicio']);
    }
}
