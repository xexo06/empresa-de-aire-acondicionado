<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tiposervicio;
use app\models\Servicio;
use app\models\Console;

/**
 * ServicioSearch represents the model behind the search form of `app\models\Servicio`.
 */
class ServicioSearch extends Servicio
{
    /**
     * @inheritdoc
     */



    public function rules()
    {
        return [
            [['nFactura', 'FK_tipoServicio','nGarantia','nServicioMantencion'], 'integer'],
            [['fecha', 'estado'], 'safe'],
            [['detalle'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$tipo)
    {
        if ($tipo != null) {
            if ($tipo == 1 || $tipo == 2) {
                $query = Servicio::find()->orWhere(['or', ['FK_tipoServicio' => 2], ['FK_tipoServicio' => 1]]);
            }
            if ($tipo == 4) {
                $consulta = "select * from servicio where nFactura in (select FK_servicio from tener where FK_equipo in(SELECT FK_equipo from garantia where estado='si')) and FK_tipoServicio=4";
                $query = Servicio::findBySql($consulta);
            }
            if ($tipo == 3) {
                $query = Servicio::find()->where(['FK_tipoServicio' => 3]);
            }
        } else {
            $query = Servicio::find();
        }

        // add conditions that should always apply here
        ?>
        <?php
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'nFactura' => $this->nFactura,
            //'fecha' => $this->fecha,
            'FK_tipoServicio' => $this->FK_tipoServicio,
            'nGarantia' => $this->nGarantia,
            'nServicioMantencion' => $this->nServicioMantencion,
            'detalle' => $this->detalle,
        ]);


        $query->andFilterWhere(['like', 'estado', $this->estado]);
        $timestamp_since = explode(";", $this->fecha, 2);
        if ($this->fecha != null) {


            $a = $timestamp_since[0];
            $b = $timestamp_since[1];

            $query->andFilterWhere(['between','fecha',$a,$b]);

         }


        /*$prueba = "2018-07-31 00:00:00;2018-07-31 23:59:59";

        $prueba2 = (string)$this->fecha;
        if ($this->fecha!= null) {

       Console::log('asd',$prueba2);
    }
        $timestamp_since = explode(";",$prueba2,2);
        $a = $timestamp_since[0];


        $query->andFilterWhere(['like','fecha',$a]);
            */


        return $dataProvider;


    }
    public function search2($params)
    {
        $query = Servicio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'nFactura' => $this->nFactura,
            'fecha' => $this->fecha,
            'FK_tipoServicio' => $this->FK_tipoServicio,
            'nGarantia' => $this->nGarantia,
            'nServicioMantencion' => $this->nServicioMantencion,
        ]);

        $query->andFilterWhere(['like', 'estado', $this->estado]);



        return $dataProvider;
    }
}
