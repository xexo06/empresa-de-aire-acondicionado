<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cuotas".
 *
 * @property string $ID
 * @property string $fechaRecibo
 * @property string $fechaPago
 * @property string $estado
 * @property int $valor
 * @property string $tipo
 * @property string $FK_pago
 *
 * @property Pago $fKPago
 */
class Cuotas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cuotas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fechaPago', 'estado', 'valor', 'tipo', 'FK_pago','fechaRecibo'], 'required'],
            [['fechaRecibo', 'fechaPago'], 'safe'],
            [['valor', 'FK_pago'], 'integer'],
            [['estado', 'tipo'], 'string', 'max' => 30],
            [['FK_pago'], 'exist', 'skipOnError' => true, 'targetClass' => Pago::className(), 'targetAttribute' => ['FK_pago' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'fechaRecibo' => 'Fecha Recibo',
            'fechaPago' => 'Fecha Pago',
            'estado' => 'Estado',
            'valor' => 'Valor',
            'tipo' => 'Tipo',
            'FK_pago' => 'ID pago',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKPago()
    {
        return $this->hasOne(Pago::className(), ['ID' => 'FK_pago']);
    }
}
