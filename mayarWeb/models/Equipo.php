<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipo".
 *
 * @property string $nSerie
 * @property string $FK_cliente
 * @property string $FK_producto
 * @property string $lng
 * @property string $lat
 *
 * @property Producto $fKProducto
 * @property Cliente $fKCliente
 * @property Garantia[] $garantias
 * @property Tener[] $teners
 */
class Equipo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'equipo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nSerie', 'FK_producto'], 'required'],
            [['FK_producto'], 'integer'],
            [['lng', 'lat'], 'string'],
            [['nSerie'], 'string', 'max' => 30],
            [['FK_cliente'], 'string', 'max' => 10],
            [['nSerie'], 'unique'],
            [['FK_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['FK_producto' => 'ID']],
            [['FK_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['FK_cliente' => 'RUT']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nSerie' => 'Serie',
            'FK_cliente' => 'Cliente',
            'FK_producto' => 'Producto',
            'lng' => 'Lng',
            'lat' => 'Lat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKProducto()
    {
        return $this->hasOne(Producto::className(), ['ID' => 'FK_producto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKCliente()
    {
        return $this->hasOne(Cliente::className(), ['RUT' => 'FK_cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGarantias()
    {
        return $this->hasMany(Garantia::className(), ['FK_equipo' => 'nSerie']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeners()
    {
        return $this->hasMany(Tener::className(), ['FK_equipo' => 'nSerie']);
    }
}
