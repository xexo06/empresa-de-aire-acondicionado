<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "garantia".
 *
 * @property string $nGarantia
 * @property string $fechaFin
 * @property string $estado
 * @property string $FK_equipo
 *
 * @property Equipo $fKEquipo
 */
class Garantia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'garantia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fechaFin', 'estado', 'FK_equipo'], 'required'],
            [['fechaFin'], 'safe'],
            [['estado'], 'string', 'max' => 10],
            [['FK_equipo'], 'string', 'max' => 30],
            [['FK_equipo'], 'exist', 'skipOnError' => true, 'targetClass' => Equipo::className(), 'targetAttribute' => ['FK_equipo' => 'nSerie']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nGarantia' => 'Garantia',
            'fechaFin' => 'Fecha Fin',
            'estado' => 'Estado',
            'FK_equipo' => 'Equipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKEquipo()
    {
        return $this->hasOne(Equipo::className(), ['nSerie' => 'FK_equipo']);
    }
}
