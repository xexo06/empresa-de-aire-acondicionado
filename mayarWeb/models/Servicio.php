<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicio".
 *
 * @property string $nFactura
 * @property string $fecha
 * @property string $FK_tipoServicio
 * @property string $estado
 * @property string $nGarantia
 * @property string $nServicioMantencion
 * @property string $RUT
 * @property Pago[] $pagos
 * @property Tiposervicio $fKTipoServicio
 * @property Tener[] $teners
 * @property Trabajar[] $trabajars
 */
class Servicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nFactura', 'fecha', 'FK_tipoServicio'], 'required'],
            [['nFactura', 'FK_tipoServicio', 'nGarantia','nServicioMantencion'], 'integer'],
            [['fecha'], 'safe'],
            [['detalle'], 'string'],
            [['estado'], 'string', 'max' => 10],
            [['nFactura'], 'unique'],
            [['FK_tipoServicio'], 'exist', 'skipOnError' => true, 'targetClass' => Tiposervicio::className(), 'targetAttribute' => ['FK_tipoServicio' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nFactura' => 'Factura',
            'fecha' => 'Fecha',
            'FK_tipoServicio' => 'Tipo Servicio',
            'detalle' => 'Detalle',
            'estado' => 'Estado',
            'nGarantia' => 'N° Cert. de Garantía',
            'nServicioMantencion' => 'N° Informe Servicio y Mantención',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagos()
    {
        return $this->hasMany(Pago::className(), ['FK_servicio' => 'nFactura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKTipoServicio()
    {
        return $this->hasOne(Tiposervicio::className(), ['ID' => 'FK_tipoServicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeners()
    {
        return $this->hasMany(Tener::className(), ['FK_servicio' => 'nFactura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajars()
    {
        return $this->hasMany(Trabajar::className(), ['FK_servicio' => 'nFactura']);
    }
}
