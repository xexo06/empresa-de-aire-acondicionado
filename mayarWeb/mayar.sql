-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 22-10-2019 a las 23:37:25
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mayar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `RUT` varchar(10) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `ciudad` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `direccion` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`RUT`, `correo`, `ciudad`, `nombre`, `telefono`, `direccion`) VALUES
('10402242-1', 'asep@gmail.com', 'sancarlos', 'alexis sepulveda', '+569 12341234', 'constitucion con libertad'),
('11319842-7', 'fran@gmail.com', 'Quirihue', 'Francisca', '+569 3345 2342', 'arauco con libertat 564'),
('11485069-1', 'isa@gmail.com', 'sancarlos', 'isabel cisternas', '+569 8686 5768', 'hongos con hojas'),
('17324791-5', 'fcar@gmail.com', 'sancarlos', 'fernando carrasco', '+569 56784567', 'bulnes con prat'),
('177572330', 'searce@alumnos.ubiobio.cl', 'sancarlos', 'Sergio Andres Arce Barrera', '+569 74623162', 'tehualda 228 villa los copihu'),
('18808479-6', 'gustavocarofuentes@gmail.com', 'san carlos', 'gustavo caro', '1245', 'castilla');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuotas`
--

CREATE TABLE `cuotas` (
  `ID` bigint(20) NOT NULL,
  `fechaRecibo` date NOT NULL,
  `fechaPago` date NOT NULL,
  `estado` varchar(30) NOT NULL,
  `valor` int(11) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `FK_pago` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cuotas`
--

INSERT INTO `cuotas` (`ID`, `fechaRecibo`, `fechaPago`, `estado`, `valor`, `tipo`, `FK_pago`) VALUES
(16, '2019-10-24', '2019-10-24', 'pendiente', 10000, 'cheque', 12),
(17, '2019-10-19', '2019-10-19', 'pendiente', 8000, 'cheque', 13),
(18, '2019-10-01', '2019-10-01', 'pendiente', 4500, 'cheque', 14),
(19, '2019-10-23', '2019-10-23', 'pagado', 10000, 'cheque', 14),
(20, '2019-10-21', '2019-10-21', 'pendiente', 4000, 'cheque', 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `nSerie` varchar(30) NOT NULL,
  `FK_cliente` varchar(10) NOT NULL,
  `FK_producto` bigint(20) NOT NULL,
  `lng` text NOT NULL,
  `lat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `equipo`
--

INSERT INTO `equipo` (`nSerie`, `FK_cliente`, `FK_producto`, `lng`, `lat`) VALUES
('1', '10402242-1', 1, ' -71.967649', '-36.419883'),
('10', '11485069-1', 2, '-71.961009', '-36.420100'),
('2', '10402242-1', 2, ' -71.967649', '-36.419883'),
('3', '11319842-7', 3, '-71.966880', '-36.424919'),
('6', '17324791-5', 3, ' -71.974188', '-36.425771'),
('7', '17324791-5', 7, '-71.963117', '-36.428264'),
('8', '177572330', 3, ' -71.958140', '-36.427644');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `garantia`
--

CREATE TABLE `garantia` (
  `nGarantia` bigint(20) NOT NULL,
  `fechaFin` date NOT NULL,
  `estado` varchar(10) NOT NULL,
  `FK_equipo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `garantia`
--

INSERT INTO `garantia` (`nGarantia`, `fechaFin`, `estado`, `FK_equipo`) VALUES
(59, '2019-10-12', 'si', '1'),
(60, '2019-10-12', 'si', '2'),
(61, '2020-10-20', 'si', '3'),
(62, '2020-10-08', 'si', '6'),
(63, '2019-10-08', 'si', '7'),
(64, '2021-01-08', 'si', '8'),
(65, '2020-10-22', 'si', '10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `ID` bigint(20) NOT NULL,
  `valorServicio` int(11) NOT NULL,
  `valorEquipo` int(11) NOT NULL,
  `interes` int(11) NOT NULL,
  `FK_servicio` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pago`
--

INSERT INTO `pago` (`ID`, `valorServicio`, `valorEquipo`, `interes`, `FK_servicio`) VALUES
(12, 5000, 5000, 0, 1),
(13, 4000, 4000, 0, 2),
(14, 4500, 10000, 0, 3),
(15, 2000, 2000, 0, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `ID` bigint(20) NOT NULL,
  `stock` int(11) NOT NULL,
  `refrigerante` varchar(30) NOT NULL,
  `modelo` varchar(30) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `capacidad` int(11) NOT NULL,
  `FK_proveedor` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`ID`, `stock`, `refrigerante`, `modelo`, `tipo`, `capacidad`, `FK_proveedor`) VALUES
(1, 427, 'dl', 'lees', 'susuki', 0, '962345769'),
(2, 57, 'hl5', 'sony', '5x', 0, '9772331-1'),
(3, 654, 'down3', 'lesgo', 'volt', 0, '8337898-0'),
(7, 503, 'ter', 'ert', 'rrr', 0, '962345769'),
(8, 20, 'refrigerante', 'marca', 'split', 18000, '8337898-0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `RUT` varchar(10) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`RUT`, `direccion`, `nombre`) VALUES
('8337898-0', 'craken con prat 122', 'air'),
('962345769', 'tehualda', 'bulnes con velasquez 456'),
('9772331-1', 'ecuador con argentina 2456', 'century');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `nFactura` bigint(20) NOT NULL,
  `fecha` datetime NOT NULL,
  `FK_tipoServicio` bigint(20) NOT NULL,
  `estado` varchar(10) DEFAULT NULL,
  `nGarantia` int(11) NOT NULL,
  `nServicioMantencion` int(11) NOT NULL,
  `detalle` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`nFactura`, `fecha`, `FK_tipoServicio`, `estado`, `nGarantia`, `nServicioMantencion`, `detalle`) VALUES
(1, '1899-12-12 09:45:00', 1, 'realizado', 1, 0, ' '),
(2, '2019-10-20 13:45:14', 2, 'realizado', 2, 0, ' '),
(3, '2020-01-08 18:30:48', 1, 'realizado', 3, 0, ' '),
(4, '2019-10-08 17:45:58', 1, 'realizado', 4, 0, ' '),
(5, '2019-10-22 18:31:20', 1, 'realizado', 5, 0, ' ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tecnico`
--

CREATE TABLE `tecnico` (
  `RUT` varchar(10) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tecnico`
--

INSERT INTO `tecnico` (`RUT`, `correo`, `telefono`, `direccion`, `nombre`) VALUES
('11877578-3', 'hermosa@gmail.com', '+569 99614455', 'puren con 18 de septiembre', 'Nicol Perez'),
('11940112-7', 'And@gmail.com', '+567 2223 3332', 'napoles 564 con italia', 'Andres torres'),
('14290928-6', 'yahoo@yahoo.cl', '92334511', '11 de septiembre 115', 'Rodrigo Cisternas'),
('188084796', 'gtavo@gmail.com', '569 99611293', 'hierbas buenas', 'gustavo caro'),
('24910422-1', 'Kari@gmail.com', '+569 3422 2311', 'brasil con libertad 567', 'karina sepulveda'),
('5421854-0', 'ttee@gmail.com', '+569 98765432', 'puentos con flores', 'sadrac fernandes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tener`
--

CREATE TABLE `tener` (
  `ID` bigint(20) NOT NULL,
  `FK_servicio` bigint(20) NOT NULL,
  `FK_equipo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tener`
--

INSERT INTO `tener` (`ID`, `FK_servicio`, `FK_equipo`) VALUES
(87, 1, '1'),
(88, 1, '2'),
(89, 2, '3'),
(90, 4, '6'),
(91, 4, '7'),
(92, 3, '8'),
(93, 5, '10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposervicio`
--

CREATE TABLE `tiposervicio` (
  `ID` bigint(20) NOT NULL,
  `tipo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tiposervicio`
--

INSERT INTO `tiposervicio` (`ID`, `tipo`) VALUES
(1, 'Instalación'),
(2, 'Venta'),
(3, 'Mantención'),
(4, 'Renovación De Garantía');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajar`
--

CREATE TABLE `trabajar` (
  `ID` bigint(20) NOT NULL,
  `FK_tecnico` varchar(10) NOT NULL,
  `FK_servicio` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `trabajar`
--

INSERT INTO `trabajar` (`ID`, `FK_tecnico`, `FK_servicio`) VALUES
(39, '5421854-0', 1),
(40, '11940112-7', 2),
(41, '24910422-1', 4),
(42, '188084796', 3),
(43, '11940112-7', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Id` int(11) NOT NULL,
  `Rut` varchar(30) NOT NULL,
  `First_name` varchar(250) NOT NULL,
  `Last_name` varchar(250) NOT NULL,
  `Telefono` varchar(30) NOT NULL,
  `Username` varchar(250) NOT NULL,
  `Email` varchar(500) NOT NULL,
  `Password` varchar(250) NOT NULL,
  `authKey` varchar(250) NOT NULL,
  `User_level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Id`, `Rut`, `First_name`, `Last_name`, `Telefono`, `Username`, `Email`, `Password`, `authKey`, `User_level`) VALUES
(1, '18213893-2', 'Sergio', 'Arce', '569 +5434 5566', 'checho', 'checho01@gmail.com', '$2y$13$Z1YdBemLbgyxzh5KA0pyYOmZx92nS6OaRYQ5hQXg1gO9wvOdc.vPy', '123', 'Administrador'),
(2, '19770772-0', 'Camila', 'Flores', '+569 5433 2211', 'camila', 'camila02@gmail.com', '$2y$13$E9Eq/9BTfqbG0FfDSN4Q5uglSnSejewwBp2eT1P37w99NGt/O77EO', '234', 'Secretaria');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`RUT`);

--
-- Indices de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_pago` (`FK_pago`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`nSerie`),
  ADD KEY `FK_cliente` (`FK_cliente`),
  ADD KEY `FK_producto` (`FK_producto`);

--
-- Indices de la tabla `garantia`
--
ALTER TABLE `garantia`
  ADD PRIMARY KEY (`nGarantia`),
  ADD KEY `FK_equipo` (`FK_equipo`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_servicio` (`FK_servicio`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_proveedor` (`FK_proveedor`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`RUT`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`nFactura`),
  ADD KEY `FK_tipoServicio` (`FK_tipoServicio`);

--
-- Indices de la tabla `tecnico`
--
ALTER TABLE `tecnico`
  ADD PRIMARY KEY (`RUT`);

--
-- Indices de la tabla `tener`
--
ALTER TABLE `tener`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_servicio` (`FK_servicio`),
  ADD KEY `FK_equipo` (`FK_equipo`);

--
-- Indices de la tabla `tiposervicio`
--
ALTER TABLE `tiposervicio`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `trabajar`
--
ALTER TABLE `trabajar`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_tecnico` (`FK_tecnico`),
  ADD KEY `FK_servicio` (`FK_servicio`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuotas`
--
ALTER TABLE `cuotas`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `garantia`
--
ALTER TABLE `garantia`
  MODIFY `nGarantia` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tener`
--
ALTER TABLE `tener`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT de la tabla `tiposervicio`
--
ALTER TABLE `tiposervicio`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `trabajar`
--
ALTER TABLE `trabajar`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuotas`
--
ALTER TABLE `cuotas`
  ADD CONSTRAINT `cuotas_ibfk_1` FOREIGN KEY (`FK_pago`) REFERENCES `pago` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `equipo_ibfk_1` FOREIGN KEY (`FK_producto`) REFERENCES `producto` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `equipo_ibfk_2` FOREIGN KEY (`FK_cliente`) REFERENCES `cliente` (`RUT`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `garantia`
--
ALTER TABLE `garantia`
  ADD CONSTRAINT `garantia_ibfk_1` FOREIGN KEY (`FK_equipo`) REFERENCES `equipo` (`nSerie`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`FK_servicio`) REFERENCES `servicio` (`nFactura`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`FK_proveedor`) REFERENCES `proveedor` (`RUT`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD CONSTRAINT `servicio_ibfk_1` FOREIGN KEY (`FK_tipoServicio`) REFERENCES `tiposervicio` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tener`
--
ALTER TABLE `tener`
  ADD CONSTRAINT `tener_ibfk_3` FOREIGN KEY (`FK_servicio`) REFERENCES `servicio` (`nFactura`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tener_ibfk_4` FOREIGN KEY (`FK_equipo`) REFERENCES `equipo` (`nSerie`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `trabajar`
--
ALTER TABLE `trabajar`
  ADD CONSTRAINT `trabajar_ibfk_2` FOREIGN KEY (`FK_tecnico`) REFERENCES `tecnico` (`RUT`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trabajar_ibfk_3` FOREIGN KEY (`FK_servicio`) REFERENCES `servicio` (`nFactura`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
