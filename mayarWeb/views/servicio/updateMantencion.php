<?php
/**
 * Created by PhpStorm.
 * User: GEST
 * Date: 22-06-2018
 * Time: 18:37
 */
/* @var $this yii\web\View */
/* @var $model app\models\Servicio */
use yii\helpers\Html;

$this->title = 'Actualizar Mantención';
$this->params['breadcrumbs'][] = ['label' => 'Mantención', 'url' => ['createmantencion']];
$this->params['breadcrumbs'][] = ['label' => 'Factura '.$model->nFactura, 'url' => ['viewmantencion', 'id' => $model->nFactura]];
$this->params['breadcrumbs'][] = 'Actualizar Mantención';
?>
<div class="servicio-update contenido">

    <div class="row">
        <div class="col-md-8">
            <h1 style="margin-top:0px;">Actualizar Mantención, Factura: <?php echo $model['nFactura'];?></h1>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::a('Ver Servicio', ['viewmantencion', 'id' => $model->nFactura], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>


    <?= $this->render('_formMantencion', [
        'model' => $model,
        'modelsEquipo' => $modelsEquipo,
        'modelsTecnico' => $modelsTecnico,
        'bloquearNFactura' => $bloquearNFactura,
    ]) ?>

</div>
