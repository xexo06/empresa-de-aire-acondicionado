<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Tecnico;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

?>

<div class="panel panel-default">
    <div class="panel-heading"><h4><i class="glyphicon glyphicon-menu-hamburger"></i> Agregar Técnicos</h4></div>
    <div class="panel-body">
        <?php  $consulta= "select * from tecnico";
        $count = Tecnico::findBySql($consulta)->count(); ?>
        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_inner',
            'widgetBody' => '.container-rooms',
            'widgetItem' => '.room-item',
            'limit' => $count,
            'min' => 1,
            'insertButton' => '.add-room',
            'deleteButton' => '.remove-room',
            'model' => $modelsTecnico[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'nombre'
            ],
        ]); ?>
        <div class="container-rooms"><!-- widgetContainer -->
            <?php foreach ($modelsTecnico as $indexRoom => $modelTecnico): ?>
                <div class="room-item panel panel-success"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Técnico</h3>
                        <div class="pull-right">
                            <button type="button" class="add-room btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-room btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                        // necessary for update action.
                        /*    if (! $modelTecnico->isNewRecord) {
                              echo Html::activeHiddenInput($modelTecnico, "[{$indexRoom}]RUT");
                          }*/
                        ?>
                        <div class="container-fluid">
                            <div class="row">
                                <?php
                                echo $form->field($modelTecnico, "[{$indexRoom}]RUT")->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(Tecnico  ::find()->all(),'RUT','nombre'),
                                    'language' => 'es',
                                    'options' => ['placeholder' => 'Selecciona una opción'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label("Nombre");
                                ?>
                            </div><!-- .row -->
                        </div>

                    </div>
                </div>
            <?php endforeach; ?>

        </div>
        <?php DynamicFormWidget::end(); ?>
    </div>

</div>