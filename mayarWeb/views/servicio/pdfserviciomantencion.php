<?php
/**
 * Created by PhpStorm.
 * User: GEST
 * Date: 26-06-2018
 * Time: 17:24
 */
use app\models\Servicio;
use app\models\Tener;
use app\models\Equipo;
use app\models\Cliente;
use app\models\Tecnico;
use app\models\Producto;
use app\models\Pago;
$servicio = Servicio::findOne($id);
$tener = Tener::find()->where(['FK_servicio' => $id])->one();
$consultaEquipo = "select * from equipo where nSerie in (select tener.FK_equipo from tener join servicio on tener.FK_servicio= servicio.nFactura where servicio.nFactura = '".$id."')";
$equipo = Equipo::findBySql($consultaEquipo)->all();
$equipo1 = Equipo::find()->where(['nSerie' => $tener->FK_equipo])->one();
$cliente = Cliente::find()->where(['RUT' => $equipo1->FK_cliente])->one();
$consultaTecnico = "select * from tecnico where RUT in (select FK_tecnico from trabajar, servicio where trabajar.FK_servicio = '".$id."')";
$tecnico = Tecnico::findBySql($consultaTecnico)->all();
?>

<p style="margin-bottom: 0px"><strong>Soc. Comercial Mayar</strong></p>
<p style="margin-bottom: 0px"><strong>Servicios y Mantención Limitada</strong></p>
<p style="margin-bottom: 0px">RUT: 76.214.071-3</p>
<p style="margin-bottom: 0px">GIRO: Contratista, Ventas de Materiales Eléctricos,</p>
<p style="margin-bottom: 0px">Electrónicos y Ferretería.</p>
<p style="margin-bottom: 0px">Chacabuco N° 721 - San Carlos</p>
<p style="margin-bottom: 0px">Fono: (42) 23311799 - Celular: 9 9434 0146</p>
<img style="height: 90px; padding-top: -140px; padding-left: 460px; padding-bottom: 140px;" src="css/icono.png" alt="">

<div style="padding-bottom: 140px">
    <p style="padding-left: 460px;  padding-top: -140px;"><?php echo 'N° '.$servicio['nServicioMantencion'];?></p>
    <br>
    <h3 align="center">INFORME DE SERVICIOS Y MANTENCIÓN</h3>
    <div style="border: 2px solid black;">
        <p style="margin-bottom: 0px; padding-left: 4px;border-bottom: 1px solid black">CLIENTE: <?php echo $cliente['nombre'];?></span></p>
        <p style="margin-bottom: 0px;padding-left: 460px;padding-top: -22px;">N° FOLIO: <?php echo $servicio['nServicioMantencion'];?></p>
        <p style="margin-bottom: 0px;padding-left: 4px;border-bottom: 1px solid black">CONTACTO: <?php echo $cliente['telefono'];?></p>
        <p style="margin-bottom: 0px;padding-left: 460px;padding-top: -21px;">FECHA: <?php echo $servicio['fecha'];?></p>
        <p style="margin-bottom: 0px;padding-left: 4px;border-bottom: 1px solid black">DIRECCIÓN: <?php echo $cliente['direccion'];?></p>
        <p style="margin-bottom: 0px;padding-left: 460px;padding-top: -21px;">MANDANTE: <?php echo $cliente['ciudad'];?></p>
        <p style="margin-bottom: 0px;padding-left: 4px">TÉCNICO:
            <?php foreach ($tecnico as $tec){
                echo $tec['nombre']. " ";
            }?>
        </p>
    </div>
    <br>
    <div style="height: 230px; border: 2px solid black;">
        <p style="padding-left: 4px;margin-bottom:18px;background: #e0e0e0;border-bottom: 1px solid black">DETALLE TRABAJO REALIZADO</p>
        <?php
            if($servicio['FK_tipoServicio'] == 4){ ?>
                <p style="padding-left: 4px">Renovación de Garantía a el/los equipo(s):
                    <?php foreach ($equipo as $e){
                        echo $e['nSerie']."/";
                    } ?>
                </p>

        <?php }else{ ?>
                <p style="padding-left: 4px"><?php echo $servicio['detalle'];?></p>
        <?php } ?>

    </div>
    <br>
    <div style="border: 2px solid black;">
        <p style="padding-left: 4px;margin-bottom:18px;background: #e0e0e0;border-bottom: 1px solid black">RECOMENDACIONES TÉCNICAS</p>
        <p style="margin-bottom:18px;border-bottom: 1px solid black"></p>
        <p style="margin-bottom:18px;border-bottom: 1px solid black"></p>
        <p style="margin-bottom:18px;border-bottom: 1px solid black"></p>
        <p style="margin-bottom:18px;border-bottom: 1px solid black"></p>
        <p style="margin-bottom:18px;border-bottom: 1px solid black"></p>
        <p style="margin-bottom:18px;border-bottom: 1px solid black"></p>
        <p style="margin-bottom:18px;border-bottom: 1px solid black"></p>
        <p style="margin-bottom:18px;border-bottom: 1px solid black"></p>
    </div>
    <br>
    <div style="border: 2px solid black;">
        <p style="padding-left: 4px;margin-bottom:18px;background: #e0e0e0;border-bottom: 1px solid black">RECEPCIÓN</p>
        <p style="margin-bottom: 4px; padding-left: 4px;">NOMBRE:______________________________________________________________</p>
        <p style="padding-left: 460px;margin-bottom: 0px;padding-top: -22px;">FIRMA:___________________________</p>
    </div>
    <br>
    <div>
        <p><img src="css/correo.png" alt="" width="25px" height="25px">&nbsp;empresamayar@hotmail.com&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="css/correo.png" alt="" width="25px" height="25px">&nbsp;ariquelme@mayar.cl&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="css/web.png" alt="" width="23px" height="23px">&nbsp;www.mayar.cl</p>
    </div>

</div>