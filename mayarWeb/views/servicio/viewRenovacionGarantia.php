<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Producto;
use app\models\Garantia;
use app\models\Tecnico;
use app\models\Cliente;
use app\models\Pago;
use app\models\Cuotas;
/* @var $this yii\web\View */
/* @var $model app\models\Servicio */

$this->title = "Factura: ".$model->nFactura;
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['createrenovaciongarantia']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-view contenido">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <p>
                    <?php  if($modelsPago ==null ){ ?>
                        <?= Html::a('Agregar Pago', ['pago/createpagoconservicio', 'id' => $model->nFactura], ['class' => 'btn btn-primary']) ?>
                    <?php }else{ ?>
                        <?= Html::a('Actualizar Pago', ['pago/updaterenovaciongarantia', 'id' => $modelsPago->ID], ['class' => 'btn btn-primary']) ?>
                    <?php } ?>
                    <?= Html::a('Actualizar', ['updaterenovaciongarantia', 'id' => $model->nFactura], ['class' => 'btn btn-warning']) ?>
                    <?= Html::a('Borrar', ['deleterenovaciongarantia', 'id' => $model->nFactura], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => '¿Está seguro que desea eliminar este servicio?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    <?= Html::a('Exportar', ['exportarpdfserviciomantencion', 'id' => $model->nFactura], ['class' => 'btn btn-success']) ?>

                </p>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
                <h3>Factura</h3>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'nFactura',
                        'fecha',
                        'nServicioMantencion',
                        'FK_tipoServicio',
                        'estado',
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <h3>Cliente</h3>
                <?= DetailView::widget([
                    'model' => $modelsClientes,
                    'attributes' => [
                        'RUT',
                        'correo',
                        'ciudad',
                        'nombre',
                        'telefono',
                        'direccion',
                    ],
                ]) ?>
            </div>
        </div>
        <?php  foreach($modelsEquipo as $Equipo ){ ?>
            <div class="col-md-12">
                <div class="col-md-4">
                    <?php
                    $consulta = "select * from producto where ID = '".$Equipo['FK_producto']."'";
                    $producto = Producto::findBySql($consulta)->one();
                    ?>
                    <h3>Producto</h3>
                    <?= DetailView::widget([
                        'model' => $producto,
                        'attributes' => [
                            'ID',
                            'stock',
                            'capacidad',
                            'refrigerante',
                            'modelo',
                            'tipo',
                            'FK_proveedor',
                        ],
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <h3>Equipo</h3>
                    <?= DetailView::widget([
                        'model' => $Equipo,
                        'attributes' => [
                            'nSerie',
                            'FK_cliente',
                            'FK_producto',
                            'lng:ntext',
                            'lat:ntext',
                        ],
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <?php
                    $sql1 = "select * from garantia where FK_equipo = '".$Equipo['nSerie']."'";
                    $modelsGarantias = Garantia::findBySql($sql1)->all();
                    ?>
                    <h3>Garantías</h3>
                    <?php  foreach($modelsGarantias as $Garantia){ ?>
                        <?= DetailView::widget([
                            'model' => $Garantia,
                            'attributes' => [
                                'nGarantia',
                                'fechaFin',
                                'estado',
                                'FK_equipo',
                            ],
                        ]) ?>
                    <?php  } ?>
                </div>
            </div>
        <?php  } ?>
    </div>
</div>



