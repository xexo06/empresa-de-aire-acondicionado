<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */

$this->title = 'Actualizar Instalación o Venta';
$this->params['breadcrumbs'][] = ['label' => 'Instalación o Venta', 'url' => ['createinstalacionventa']];
$this->params['breadcrumbs'][] = ['label' => 'Factura '.$model->nFactura, 'url' => ['viewinstalacionventa', 'id' => $model->nFactura]];
$this->params['breadcrumbs'][] = 'Actualizar Factura';
?>
<div class="servicio-update contenido">
    <div class="row">
        <div class="col-md-8">
            <h1 style="margin-top:0px;">Actualizar Instalación o Venta, Factura: <?php echo $model['nFactura'];?></h1>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::a('Ver Servicio', ['viewinstalacionventa', 'id' => $model->nFactura], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>


    <?= $this->render('_formInstalacionVenta', [
        'model' => $model,
        'bloquearNFactura' => $bloquearNFactura,
        'modelsPago' => $modelsPago,
        'modelsEquipo' => $modelsEquipo,
        'modelsTecnico' => $modelsTecnico,
        'cliente' => $cliente,
    ]) ?>

</div>
