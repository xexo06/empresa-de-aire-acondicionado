<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Producto;
use app\models\Tecnico;
use app\models\Cliente;
use app\models\Pago;
use app\models\Cuotas;
/* @var $this yii\web\View */
/* @var $model app\models\Servicio */

$this->title = "Factura: ".$model->nFactura;
$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['createinstalacionventa']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-view contenido">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <p>

                    <?php  if($modelsPago ==null ){ ?>
                        <?= Html::a('Agregar Pago', ['pago/createpagoconservicio', 'id' => $model->nFactura], ['class' => 'btn btn-primary']) ?>
                    <?php }else{ ?>
                        <?= Html::a('Actualizar Pago', ['pago/updatepago', 'id' => $modelsPago->ID], ['class' => 'btn btn-primary']) ?>
                    <?php } ?>
                    <?= Html::a('Actualizar', ['updateinstalacionventa', 'id' => $model->nFactura], ['class' => 'btn btn-warning']) ?>
                    <?= Html::a('Eliminar', ['deleteinstalacionventa', 'id' => $model->nFactura], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => '¿Está seguro que desea eliminar este servicio?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    <?= Html::a('Exportar', ['exportarpdfinstalacion', 'id' => $model->nFactura], ['class' => 'btn btn-success']) ?>

                </p>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
                <h3>Factura</h3>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'nFactura',
                        'fecha',
                        'nGarantia',
                        'FK_tipoServicio',
                        'estado',
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <h3>Cliente</h3>
                <?= DetailView::widget([
                    'model' => $cliente,
                    'attributes' => [
                        'RUT',
                        'correo',
                        'ciudad',
                        'nombre',
                        'telefono',
                        'direccion',
                    ],
                ]) ?>
            </div>
        </div>
        <?php
            foreach ($modelsEquipo as $e){
                $consulta = "select * from producto where ID = '".$e['FK_producto']."'";
                $producto = Producto::findBySql($consulta)->one();
                ?>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <h3>Producto</h3>
                        <?= DetailView::widget([
                            'model' => $producto,
                            'attributes' => [
                                'ID',
                                'stock',
                                'capacidad',
                                'refrigerante',
                                'modelo',
                                'tipo',
                                'FK_proveedor',
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <h3>Equipo</h3>
                        <?= DetailView::widget([
                            'model' => $e,
                            'attributes' => [
                                'nSerie',
                                'FK_producto',
                                'lng:ntext',
                                'lat:ntext',
                            ],
                        ]) ?>
                    </div>
                </div>
        <?php
            }
        ?>
        <div class="col-md-12">
            <?php  foreach($modelsTecnicos as $Tecnico ){ ?>
            <div class="col-md-6">
                <h3>Técnico</h3>
                <?= DetailView::widget([
                    'model' => $Tecnico,
                    'attributes' => [
                        'RUT',
                        'correo',
                        'telefono',
                        'direccion',
                        'nombre',
                    ],
                ]) ?>
            </div>
            <?php  } ?>
        </div>

        <?php  if($modelsPago !=null ){ ?>
            <div class="col-md-12">
                <div class="col-md-6">
                    <h3>Pago</h3>
                    <?= DetailView::widget([
                        'model' => $modelsPago,
                        'attributes' => [
                            'ID',
                            'valorServicio',
                            'valorEquipo',
                            'interes',
                            'FK_servicio',
                        ],
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <h3>Cuotas</h3>
                    <?php  foreach($modelsCuotas as $cuota ){ ?>
                        <?= DetailView::widget([
                            'model' => $cuota,
                            'attributes' => [
                                'ID',
                                'fechaRecibo',
                                'fechaPago',
                                'estado',
                                'valor',
                                'tipo',
                                'FK_pago',
                            ],
                        ]) ?>
                    <?php  } ?>
                </div>
            </div>
        <?php  } ?>
    </div>


</div>

