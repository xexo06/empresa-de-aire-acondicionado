<?php
/**
 * Created by PhpStorm.
 * User: GEST
 * Date: 25-06-2018
 * Time: 14:05
 */
use yii\helpers\Html;
use dosamigos\datetimepicker\DateTimePicker;
use yii\widgets\ActiveForm;
use app\models\Servicio;
$this->title = 'Servicios Periódicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-periodico">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <?php $form = ActiveForm::begin(['action' => 'index.php?r=servicio%2Fservicioperiodicofecha', 'method' => 'GET']); ?>
        <div class="col-md-3">
            <?php
            echo '<label>Fecha Inicio</label>';
            echo DateTimePicker::widget([
                'name' => 'fecha1',
                'attribute' => 'fecha',
                'options' => ['placeholder' => 'Ingresar'],
                'language' => 'es',
                'size' => 'ms',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd HH:ii:ss P',
                    'todayBtn' => true
                ]
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <?php
            echo '<label>Fecha Fin</label>';
            echo DateTimePicker::widget([
                'name' => 'fecha2',
                'attribute' => 'fecha',
                'options' => ['placeholder' => 'Ingresar'],
                'language' => 'es',
                'size' => 'ms',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd HH:ii:ss P',
                    'todayBtn' => true
                ]
            ]);
            ?>
        </div>
        <div class="col-md-3" style="padding-top :25px">
            <?= Html::submitButton('Filtrar', ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <br>
    <hr>
    <br>
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <ul class="list-group">
                <li class="list-group-item active">Cantidad</li>
                <?php

                foreach ($modelTiposervicio as $mts){
                    $consulta="SELECT * FROM servicio WHERE FK_tipoServicio = '".$mts['ID']."' AND fecha BETWEEN '".$fechaInicio."' AND '".$fechaFin."' AND estado = 'realizado'";
                    $count = Servicio::findBySql($consulta)->count();
                    ?>
                    <li class="list-group-item">
                        <span class="badge"><?php echo $count; ?></span>
                        <?php echo $mts['tipo']?>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#w1").val('<?php echo $fechaInicio;?>');
        $("#w2").val('<?php echo $fechaFin;?>');
    });
</script>