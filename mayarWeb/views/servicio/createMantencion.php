<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\Tecnico;
use yii\grid\GridView;

use kartik\daterange\DateRangePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Servicio */

$this->title = 'Crear Mantención';
$this->params['breadcrumbs'][] = "Mantención";
?>
<div class="servicio-create contenido">
    <div class="row">
        <div class="col-md-6">
            <h1 style="margin-top:0px">Mantención</h1>
        </div>
        <div class="col-md-6">
            <div class="pull-right">
                <!-- Button trigger modal -->
                <?= Html::button('Crear Técnico', ['id' => 'modelButtontecnico', 'value' =>Url::to('index.php?r=tecnico%2Fcreateajax'), 'class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>



    <?= $this->render('_formMantencion', [
        'model' => $model,
        'modelsEquipo' =>$modelsEquipo,
        'modelsTecnico'=>$modelsTecnico,
        'bloquearNFactura' => $bloquearNFactura,
    ]) ?>

</div>

<?php
Modal::begin([
'header' => '<h3 class="tituloModal" align="center">Crear Técnico</h3>',
'id'     => 'modal2',
'size'   => 'modal-md',
]);

echo "<div id='modelContent2'></div>";

Modal::end();
?>
<br>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'nFactura',
         [
            'attribute' => 'fecha',
            'value' => function ($data) {
                 $nueva=explode(":",$data->fecha);
                return $nueva[0].":".$nueva[1];
            },

             'filter' => DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'fecha',
                'convertFormat' => true,
                'presetDropdown'=>true,

                'pluginOptions' => [
                    'locale' => [
                        'format' => 'Y-m-d H:i',
                        'separator' => ';',
                        'opens' => 'left'
                    ]
                ]
            ])

        ],
        'nServicioMantencion',
        [
            'attribute' => 'FK_tipoServicio',
            'value' => 'fKTipoServicio.tipo'
        ],
        [
            'attribute' => 'estado',
            'filter'=>array("realizado"=>"realizado","pendiente"=>"pendiente")
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Acciones',
            'template' => '{view}{update}{delete}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                        'title' => Yii::t('app', 'Ver'),
                    ]);
                },

                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'title' => Yii::t('app', 'Actualizar'),
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'title' => Yii::t('app', 'Eliminar'),
                        'class' => 'eliminar',
                        'id' => $model->nFactura,
                        'data' => [
                            'confirm' => '¿Está seguro que desea eliminar este servicio?'
                        ]
                    ]);
                }

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url ='index.php?r=servicio%2Fviewmantencion&id='.$model->nFactura;
                    return $url;
                }

                if ($action === 'update') {
                    $url ='index.php?r=servicio%2Fupdatemantencion&id='.$model->nFactura;
                    return $url;
                }
                if ($action === 'delete') {
                    $url ='index.php?r=servicio%2Fdeletemantencion&id='.$model->nFactura;
                    return $url;
                }

            }
        ],
    ],
]); ?>

