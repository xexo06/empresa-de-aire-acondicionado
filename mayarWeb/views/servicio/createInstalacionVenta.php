<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\grid\GridView;


use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */

$this->title = 'Crear Servicio';
$this->params['breadcrumbs'][] = "Instalación o venta";
?>
<div class="servicio-create contenido">
    <div class="row">
        <div class="col-md-6">
            <h1 style="margin-top:0px">Instalación o Venta</h1>
        </div>
        <div class="col-md-6">
            <div class="pull-right">
                <!-- Button trigger modal -->
                <?= Html::button('Crear Técnico', ['id' => 'modelButtontecnico', 'value' =>Url::to('index.php?r=tecnico%2Fcreateajax'), 'class' => 'btn btn-primary']) ?>
                <?= Html::button('Crear Cliente', ['id' => 'modelButtoncliente', 'value' =>Url::to('index.php?r=cliente%2Fcreateajax'), 'class' => 'btn btn-warning']) ?>
                <?= Html::button('Crear Producto', ['id' => 'modelButtonproducto', 'value' =>Url::to('index.php?r=producto%2Fcreateajax'), 'class' => 'btn btn-success']) ?>
                <?= Html::button('Crear Proveedor', ['id' => 'modelButtonproveedor', 'value' =>Url::to('index.php?r=proveedor%2Fcreateajax'), 'class' => 'btn btn-danger']) ?>

            </div>
        </div>
    </div>
    <?= $this->render('_formInstalacionVenta', [
        'model' => $model,
        'bloquearNFactura' => $bloquearNFactura,
        'cliente' => $cliente,
        'modelsEquipo' =>$modelsEquipo,
        'modelsTecnico'=>$modelsTecnico,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
    <br>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'nFactura',
        [
            'attribute' => 'fecha',

            'value' => function ($data) {
                 $nueva=explode(":",$data->fecha);
                return $nueva[0].":".$nueva[1];
            },



            'filter' => DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'fecha',
                'convertFormat' => true,
                'presetDropdown'=>true,

                'pluginOptions' => [
                    'locale' => [
                        'format' => 'Y-m-d H:i',
                        'separator' => ';',
                        'opens' => 'left'
                    ]
                ]
            ])

        ],
        'nGarantia',
        [

            'attribute' => 'FK_tipoServicio',
            'value' => 'fKTipoServicio.tipo',
            'filter'=>array("1"=>"Instalación","2"=>"Venta")
        ],
        [
            'attribute' => 'estado',
            'filter'=>array("realizado"=>"realizado","pendiente"=>"pendiente")
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Acciones',
            'template' => '{view}{update}{delete}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                        'title' => Yii::t('app', 'Ver'),
                    ]);
                },

                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'title' => Yii::t('app', 'Actualizar'),
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'title' => Yii::t('app', 'Eliminar'),
                        'class' => 'eliminar',
                        'id' => $model->nFactura,
                        'data' => [
                            'confirm' => '¿Está seguro que desea eliminar este servicio?'
                        ]
                    ]);
                }

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url ='index.php?r=servicio%2Fviewinstalacionventa&id='.$model->nFactura;
                    return $url;
                }

                if ($action === 'update') {
                    $url ='index.php?r=servicio%2Fupdateinstalacionventa&id='.$model->nFactura;
                    return $url;
                }
                if ($action === 'delete') {
                    $url ='index.php?r=servicio%2Fdeleteinstalacionventa&id='.$model->nFactura;
                    return $url;
                }

            }
        ],
    ],
]); ?>
<?php
Modal::begin([
    'header' => '<h3 class="tituloModal" align="center">Crear Cliente</h3>',
    'id'     => 'modal',
    'size'   => 'modal-md',
]);

echo "<div id='modelContent'></div>";

Modal::end();

Modal::begin([
    'header' => '<h3 class="tituloModal" align="center">Crear Técnico</h3>',
    'id'     => 'modal2',
    'size'   => 'modal-md',
]);

echo "<div id='modelContent2'></div>";

Modal::end();
Modal::begin([
    'header' => '<h3 class="tituloModal" align="center">Crear Producto</h3>',
    'id'     => 'modal3',
    'size'   => 'modal-md',
]);

echo "<div id='modelContent3'></div>";

Modal::end();

Modal::begin([
    'header' => '<h3 class="tituloModal" align="center">Crear Proveedor</h3>',
    'id'     => 'modal4',
    'size'   => 'modal-md',
]);

echo "<div id='modelContent4'></div>";
Modal::end();

?>


