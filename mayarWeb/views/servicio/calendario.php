<?php
use yii\helpers\Html;

use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReservaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Calendario';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reserva-index">



    <h1>Calendario</h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= yii2fullcalendar\yii2fullcalendar::widget(array('events'=>$events,'id' => 'calendar'));
    ?>

</div>
<script>
    $('#calendar').fullCalendar({
        eventClick: function(calEvent, jsEvent, view) {

            alert('Event: ' + calEvent.title);


        }
    });
</script>