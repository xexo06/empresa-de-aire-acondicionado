<?php
/**
 * Created by PhpStorm.
 * User: GEST
 * Date: 22-06-2018
 * Time: 20:01
 */
use yii\helpers\Html;
$this->title = 'Actualizar Renovación de Garantía';
$this->params['breadcrumbs'][] = ['label' => 'Garantías', 'url' => ['createrenovaciongarantia']];
$this->params['breadcrumbs'][] = ['label' => 'Factura '.$model->nFactura, 'url' => ['viewrenovaciongarantia', 'id' => $model->nFactura]];
$this->params['breadcrumbs'][] = 'Actualizar Renovación de Garantía';
?>
<div class="servicio-update contenido">

    <div class="row">
        <div class="col-md-8">
            <h1 style="margin-top:0px;">Actualizar Renovación de Garantía, Factura: <?php echo $model['nFactura'];?></h1>
        </div>
        <div class="col-md-4">
            <div class="pull-right">
                <?= Html::a('Ver Servicio', ['viewrenovaciongarantia', 'id' => $model->nFactura], ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?= $this->render('_formRenovaciongarantia', [
        'model' => $model,
        'Equipo' => $Equipo,
        'bloquearNFactura' => $bloquearNFactura,
        'modelsEquipo' => $modelsEquipo,
    ]) ?>

</div>