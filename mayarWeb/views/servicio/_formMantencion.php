<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datetimepicker\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Tiposervicio;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\Equipo;
use app\models\Garantia;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
/*
$Losproducto=Producto::find()->all();
$productosRenovables=[new Producto];

foreach ($Losproducto as $data){

    $producto=$data['numeroSerie'];

    $cantidadGarantias = Garantia::find()
        ->where(['ID_producto' =>$producto])
        ->count();


    $countFechaVigente = Garantia::find()
        ->where("(ID_producto ='$producto' AND fecha_fin>=CURDATE())")
        ->count();
    if($cantidadGarantias!=3 && $countFechaVigente==0){
        $productosRenovables=$data;

    }
}
*/
?>







<div class="servicio-form panel-guion">
    <?php $form = ActiveForm::begin(['id'=>'dynamic-form']); ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading"><h4><i class="glyphicon glyphicon-menu-hamburger"></i> Agregar Factura</h4></div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <?= $form->field($model, 'nFactura')->textInput(['maxlength' => true,'disabled' => $bloquearNFactura,'type' => 'number', 'min' => 1]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'nServicioMantencion')->textInput(['maxlength' => true,'type' => 'number', 'min' => 1]); ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'detalle')->textarea(['rows' => '6']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'fecha')->widget(DateTimePicker::className(), [
                            'language' => 'es',
                            'size' => 'ms',
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd  HH:ii P',
                                'todayBtn' => true
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-heading"><h4><i class="glyphicon glyphicon-menu-hamburger"></i> Agregar Equipos</h4></div>
                <div class="panel-body">
                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "" [A-Za-z0-9]
                        'widgetBody' => '.container-items', // required: css class selector
                        'widgetItem' => '.item', // required: css cla
                        'min' => 1, // 0 or 1 (default 1)
                        'insertButton' => '.add-item', // css class
                        'deleteButton' => '.remove-item', // css class
                        'model' => $modelsEquipo[0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'numeroSerie',
                        ],
                    ]); ?>

                    <div class="container-items"><!-- widgetContainer -->
                        <?php foreach ($modelsEquipo as $i => $modelEquipo): ?>
                            <div class="item panel panel-success"><!-- widgetBody -->
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Equipo</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <?php

                                            echo $form->field($modelEquipo, "[{$i}]nSerie")->widget(Select2::classname(), [
                                                'data' => ArrayHelper::map(Equipo::find()->all(), 'nSerie', 'nSerie'),
                                                'language' => 'es',
                                                'options' => ['placeholder' => 'Selecciona una opción'],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],
                                            ]);
                                            ?>
                                        </div><!-- .row -->
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php DynamicFormWidget::end(); ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <?= $this->render('_form-tecnicos', [
                'form' => $form,
                'modelsTecnico' => $modelsTecnico,
            ]) ?>

        </div>
    </div>

    <div class="form-group col-lg-offset-11">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success btn-block btn-lg']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $("#dynamic-form").submit(function() {
        //$('#servicio-nfactura').val()
        var idFactura = $('#servicio-nfactura').val();
        var fecha = $('#servicio-fecha').val();
        var nServicioMantencion= $('#servicio-nserviciomantencion').val();
        if(idFactura.length >0 && fecha.length ==0 && nServicioMantencion.length == 0){
            window.location.href = "index.php?r=servicio%2Fupdateverificacion&id="+idFactura+"&servicio=mantencion";
        }
    });
</script>