<?php
/**
 * Created by PhpStorm.
 * User: GEST
 * Date: 26-06-2018
 * Time: 17:24
 */
use app\models\Servicio;
use app\models\Tener;
use app\models\Equipo;
use app\models\Cliente;
use app\models\Producto;
use app\models\Pago;
$servicio = Servicio::findOne($id);
$tener = Tener::find()->where(['FK_servicio' => $id])->one();
$consultaEquipo = "select * from equipo where nSerie in (select tener.FK_equipo from tener join servicio on tener.FK_servicio= servicio.nFactura where servicio.nFactura = '".$id."')";
$equipo = Equipo::findBySql($consultaEquipo)->all();
$equipo1 = Equipo::find()->where(['nSerie' => $tener->FK_equipo])->one();
$cliente = Cliente::find()->where(['RUT' => $equipo1->FK_cliente])->one();
?>

<p style="margin-bottom: 0px"><strong>Soc. Comercial Mayar</strong></p>
<p style="margin-bottom: 0px"><strong>Servicios y Mantención Limitada</strong></p>
<p style="margin-bottom: 0px">RUT: 76.214.071-3</p>
<p style="margin-bottom: 0px">GIRO: Contratista, Ventas de Materiales Eléctricos,</p>
<p style="margin-bottom: 0px">Electrónicos y Ferretería.</p>
<p style="margin-bottom: 0px">Chacabuco N° 721 - San Carlos</p>
<p style="margin-bottom: 0px">Fono: (42) 23311799 - Celular: 9 9434 0146</p>

<img style="height: 90px; padding-top: -140px; padding-left: 460px; padding-bottom: 140px;" src="css/icono.png" alt="">
<div style="padding-bottom: 140px">
    <p style="padding-left: 460px;  padding-top: -140px;"><?php echo 'N° '.$servicio['nGarantia'];?></p>
    <br>
    <h3 align="center">CERTIFICADO DE GARANTÍA</h3>
    <?php
        $fecha = date_create($servicio['fecha']);
        $fechaFormateada =  date_format($fecha, 'Y-m-d');
    ?>
    <p style="margin-bottom: 0px">Fecha: <?php echo $fechaFormateada;?></p>
    <p style="margin-bottom: 0px;padding-left: 460px;padding-top: -20px;">N° Contacto: <?php echo $cliente['telefono'];?></p>
    <p>Señor(a): <?php echo $cliente['nombre'];?></p>
    <p style="padding-left: 380px;padding-top: -30px;">Correo: <?php echo $cliente['correo'];?></p>
    <p>Por medio del presente hacemos entrega de la Garantía de Suministro de equipo(s) de aire acondicionado junto con su control remoto y manuales de uso e instalación.</p>
    <p>Instalado en: <?php echo $cliente['direccion'];?></p>
    <p style="padding-left: 500px;padding-top: -30px;">Ciudad: <?php echo $cliente['ciudad'];?></p>
    <p>Características de los equipos</p>
    <table style="width:100%;">
        <tr>
            <th style="border: 1px solid black;">Cantidad</th>
            <th style="border: 1px solid black;">Tipo</th>
            <th style="border: 1px solid black;">Capacidad</th>
            <th style="border: 1px solid black;">Marca</th>
            <th style="border: 1px solid black;">Refrigerante</th>
            <th style="border: 1px solid black;">N° Serie</th>
        </tr>
        <?php
        foreach ($equipo as $e){
            $consulta = "select * from producto where ID = ".$e['FK_producto'];
            $p = Producto::findBySql($consulta)->one();

            $consulta3="select equi.nSerie from equipo equi  INNER JOIN producto pro INNER JOIN tener te where equi.FK_producto=pro.ID and te.FK_equipo=equi.nSerie and equi.FK_producto=".$e['FK_producto']." and te.FK_servicio=".$id;
            $count1 = Producto::findBySql($consulta3)->count();

            echo "<tr>";
            echo "<td style=\"border: 1px solid black;\">" . $count1. "</td>";
            echo "<td style=\"border: 1px solid black;\">" . $p['tipo'] . "</td>";
            echo "<td style=\"border: 1px solid black;\">" . $p['capacidad'] . "</td>";
            echo "<td style=\"border: 1px solid black;\">" . $p['modelo'] . "</td>";
            echo "<td style=\"border: 1px solid black;\">" . $p['refrigerante'] . "</td>";
            echo "<td style=\"border: 1px solid black;\">" . $e['nSerie'] . "</td>";
            echo "</tr>";
        }
        ?>
    </table>
    <?php
        $consulta = "select * from pago where FK_servicio = ".$id;
        $p = Pago::findBySql($consulta)->one();
    ?>
    <br>
    <?php $suma = $p['valorServicio'] + $p['valorEquipo'] + $p['interes']?>
    <p>Modalidad de Pago: <?php echo $suma; ?></p>
    <p style="padding-left: 250px;padding-top: -30px;">Orden de Compra: </p>
    <p style="padding-left: 480px;padding-top: -30px;">N° Factura/Boleta: <?php echo $servicio['nFactura'];?></p>
    <p style="margin-bottom: 0px"><strong>Garantías</strong></p>
    <p align="justify" >Todos los equipos cuentan con una garantía por defecto de fabricación, en componentes de las unidades dentro del uso normal de las mismas,
    la cual consiste en el cambio de piezas que resulten defectuosas y permanecerá vigente por un periodo de 12 meses a partir de esta fecha.
    Esta garantía no incluye mano de obra o traslados, sólo respuestas dañados o defectuosos.</p>
    <p style="margin-bottom: 0px"><strong>Condiciones para reclamar la garantía</strong></p>
    <p style="margin-bottom: 0px">1.Certificado de garantía respectivo a Mayar Ltda.</p>
    <p style="margin-bottom: 0px" align="justify">2.El suministro de energía eléctrica debe ser de 220 V. (garantía no cubre defectos por alza de energía eléctrica).</p>
    <p style="margin-bottom: 0px">3.Control remoto no gozan de garantía por golpe, caída, humedad o un mal uso de este.</p>
    <p style="margin-bottom: 0px" align="justify">4.Daños o reparaciones necesarias como consecuencia de fallas en instalaciones eléctricas, o malas aplicaciones.</p>
    <p style="margin-bottom: 0px">5.Abuso y alteraciones no autorizadas o inadecuado servicio de operación.</p>
    <p style="margin-bottom: 0px" align="justify">6.No se admitirá reclamos en piezas removidas de nuestros equipos sin la previa autorización de nuestro Departamento Técnico</p>
    <p style="margin-bottom: 0px">7.Garantía no cubre traslados o reubicación de equipo (unidad interior o exterior).</p>
    <p style="margin-bottom: 0px">8.No se aceptarán reclamos de instalación posteriores a la recepción del trabajo realizado</p>
    <p style="margin-bottom: 0px">9.Garantía no cubre mantenciones</p>
    <br>

    <p style="padding-left: 10px;">____________________________</p>
    <p style="padding-left: 73px;"><strong>Cliente</strong></p>
    <p style="padding-left: 41px;"><strong>Recibo Conforme</strong></p>
    <p style="padding-left: 500px;padding-top: -78px;">____________________________</p>
    <p style="padding-left: 555px;"><strong>Mayar Ltda</strong></p>
    <p style="padding-left: 526px; margin-bottom: 0px   "><strong>RUT: 76.214.071-3</strong></p>
</div>

