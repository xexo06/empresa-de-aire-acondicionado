<?php

use yii\helpers\Html;
use yii\grid\GridView;

use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */

$this->title = 'Renovar Garantía';
$this->params['breadcrumbs'][] = "Renovar Garantía";
?>
<div class="servicio-create contenido">
    <div class="row">
        <div class="col-md-6">
            <h1 style="margin-top:0px">Renovar garantía</h1>
        </div>
        <div class="col-md-6">
            <div class="pull-right">
                <!-- Button trigger modal -->

            </div>
        </div>
    </div>

    <?= $this->render('_formRenovaciongarantia', [
        'model' => $model,
        'bloquearNFactura' => $bloquearNFactura,
        'Equipo' => $equipo,
        'modelsEquipo' =>$modelsEquipo,

    ]) ?>

</div>
    <br>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'nFactura',
        [
            'attribute' => 'fecha',
            'value' => function ($data) {
                $nueva=explode(":",$data->fecha);
                return $nueva[0].":".$nueva[1];
            },

            'filter' => DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'fecha',
                'convertFormat' => true,
                'presetDropdown'=>true,

                'pluginOptions' => [
                    'locale' => [
                        'format' => 'Y-m-d H:i',
                        'separator' => ';',
                        'opens' => 'left'
                    ]
                ]
            ])

        ],
        'nServicioMantencion',
        [
            'attribute' => 'FK_tipoServicio',
            'value' => 'fKTipoServicio.tipo'
        ],
        'estado',

        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Acciones',
            'template' => '{view}{update}{delete}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                        'title' => Yii::t('app', 'Ver'),
                    ]);
                },

                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'title' => Yii::t('app', 'Actualizar'),
                    ]);
                },
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'title' => Yii::t('app', 'Eliminar'),
                        'class' => 'eliminar',
                        'id' => $model->nFactura,
                        'data' => [
                            'confirm' => '¿Está seguro que desea eliminar este servicio?'
                        ]
                    ]);
                }

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url ='index.php?r=servicio%2Fviewrenovaciongarantia&id='.$model->nFactura;
                    return $url;
                }

                if ($action === 'update') {
                    $url ='index.php?r=servicio%2Fupdaterenovaciongarantia&id='.$model->nFactura;
                    return $url;
                }
                if ($action === 'delete') {
                    $url ='index.php?r=servicio%2Fdeleterenovaciongarantia&id='.$model->nFactura;
                    return $url;
                }

            }
        ],
    ],
]); ?>