<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\helpers\Url;
use yii\bootstrap\Modal;

use app\models\Proveedor;
use app\models\Producto;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProveedorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proveedor';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proveedor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Crear Proveedor', ['id' => 'modelButtonproveedor', 'value' =>Url::to('index.php?r=proveedor%2Fcreateajax'), 'class' => 'btn btn-success']) ?>

    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'RUT',
            'direccion',
            'nombre',

           [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'Ver'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Actualizar'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Eliminar'),
                            'class' => 'eliminar',
                            'data' => [
                                'confirm' => '¿Está seguro que desea eliminar este proveedor?'
                            ]
                        ]);
                    }
                ],


                    'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=proveedor%2Fview&id='.$model->RUT;
                        return $url;
                    }
                    if ($action === 'update') {
                        $url ='index.php?r=proveedor%2Fupdate&id='.$model->RUT;
                        return $url;
                    }
                    if ($action === 'delete') {

                        $sql = "SELECT * FROM producto WHERE FK_proveedor='".$model->RUT."' ";
                        $Productos =Producto::findBySql($sql)->count();


                        if($Productos>0) {
                            $url = 'index.php?r=proveedor%2Fdelete&id=1';
                            return $url;

                        }else{
                            $url = 'index.php?r=proveedor%2Fdelete&id='.$model->RUT;
                            return $url;
                        }

                    }

                }





            ],

        ],
    ]); ?>
</div>

<?php
Modal::begin([
'header' => '<h4 class="tituloModal" align="center">Ingresar Proveedor</h4>',
'id'     => 'modal4',
'size'   => 'modal-md',
]);

echo "<div id='modelContent4'></div>";

Modal::end();

?>