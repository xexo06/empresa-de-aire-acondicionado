<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Proveedor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedor-form">

    <?php $form = ActiveForm::begin(['action' => 'index.php?r=proveedor%2Fcreateajax', 'id' => $model->formName()]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'RUT')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'correo')->textInput(['maxlength' => true]) ?>
        </div>

    </div>
    <div class="row">
        <hr>
        <div class="col-md-12">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success col-md-offset-10']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
<?php
//beforeSubmit
$js = "
        $('form#".$model->formName()."').on('beforeSubmit', function(e){
            var \$form = $(this);
            if( Rut($('#proveedor-rut').val())){
                submitMySecondForm(\$form);
            }
        }).on('submit', function(e){
            e.preventDefault();
        });";
$this->registerJs($js);
?>