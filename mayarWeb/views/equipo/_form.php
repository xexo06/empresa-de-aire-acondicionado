<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Cliente;
use app\models\Producto;

/* @var $this yii\web\View */
/* @var $model app\models\Equipo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="equipo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nSerie')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'FK_cliente')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Cliente::find()->all(),'RUT','nombre'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecciona una opción'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?php
    echo $form->field($model, 'FK_producto')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Producto::find()->all(),'ID','ID'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecciona una opción'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'lat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lng')->textarea(['rows' => 6]) ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
