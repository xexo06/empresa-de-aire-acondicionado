<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use dosamigos\datetimepicker\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Pago;

/* @var $this yii\web\View */
/* @var $model app\models\Cuotas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuotas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo '<label>Fecha Recibo</label>';
    echo DateTimePicker::widget([
        'model' => $model,
        'attribute' => 'fechaRecibo',
        'language' => 'es',
        'size' => 'ms',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd P',
            'todayBtn' => true
        ]
    ]);?>


    <?php
    echo '<label>Fecha Pago</label>';
    echo DateTimePicker::widget([
        'model' => $model,
        'attribute' => 'fechaPago',
        'language' => 'es',
        'size' => 'ms',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd P',
            'todayBtn' => true
        ]
    ]);?>

    <?= $form->field($model, 'estado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'valor')->textInput() ?>

    <?= $form->field($model, 'tipo')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'FK_pago')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Pago::find()->all(),'ID','ID'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecciona una opción'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
