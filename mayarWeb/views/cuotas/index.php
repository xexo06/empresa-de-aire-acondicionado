<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CuotasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cuotas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cuotas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Cuotas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'fechaRecibo',
            'fechaPago',
            'estado',
            'valor',
            //'tipo',
            //'FK_pago',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
