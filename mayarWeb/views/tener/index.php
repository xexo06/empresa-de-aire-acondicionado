<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TenerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Teners';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tener-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tener', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'FK_servicio',
            'FK_equipo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
