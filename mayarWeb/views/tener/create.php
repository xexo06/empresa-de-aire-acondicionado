<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tener */

$this->title = 'Create Tener';
$this->params['breadcrumbs'][] = ['label' => 'Teners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tener-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
