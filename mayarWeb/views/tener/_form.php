<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Equipo;
use app\models\Servicio;


/* @var $this yii\web\View */
/* @var $model app\models\Tener */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tener-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'FK_equipo')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Equipo::find()->all(),'nSerie','nSerie'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecciona una opción'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?php
    echo $form->field($model, 'FK_servicio')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Servicio::find()->all(),'nFactura','nFactura'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecciona una opción'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
