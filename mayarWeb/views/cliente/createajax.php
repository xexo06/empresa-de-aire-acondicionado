<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cliente */

?>
<div class="cliente-create">

    <?= $this->render('_formajax', [
        'model' => $model,
    ]) ?>

</div>