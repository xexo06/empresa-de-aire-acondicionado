<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\helpers\Url;
use yii\bootstrap\Modal;

use app\models\Cliente;
use app\models\Equipo;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ClienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Crear Cliente', ['id' => 'modelButtoncliente', 'value' =>Url::to('index.php?r=cliente%2Fcreateajax'), 'class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'RUT',
            'correo',
            'ciudad',
            'nombre',
            'telefono',
            //'direccion',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'Ver'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Actualizar'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Eliminar'),
                            'class' => 'eliminar',
                            'data' => [
                                'confirm' => '¿Está seguro que desea eliminar este cliente?'
                            ]
                        ]);
                    }
                ],


                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=cliente%2Fview&id='.$model->RUT;
                        return $url;
                    }
                    if ($action === 'update') {
                        $url ='index.php?r=cliente%2Fupdate&id='.$model->RUT;
                        return $url;
                    }
                    if ($action === 'delete') {

                        $sql = "SELECT * FROM equipo WHERE FK_cliente='".$model->RUT."' ";
                        $Equipo =Equipo::findBySql($sql)->count();


                        if($Equipo>0) {
                            $url = 'index.php?r=cliente%2Fdelete2&id=1';
                            return $url;

                            }else{
                            $url = 'index.php?r=cliente%2Fdelete2&id='.$model->RUT;
                            return $url;
                        }

                    }

                }


            ],
        ],
    ]); ?>
</div>

<?php

Modal::begin([
    'header' => '<h3 class="tituloModal" align="center">Ingresar Cliente</h3>',
    'id'     => 'modal',
    'size'   => 'modal-md',
]);

echo "<div id='modelContent'></div>";

Modal::end();
