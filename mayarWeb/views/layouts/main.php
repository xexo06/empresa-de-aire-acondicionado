<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Usuarios;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if(Yii::$app->user->isGuest){
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Login', 'url' => ['/site/login']]

            ],
        ]);
    }else {
        if (Yii::$app->user->identity->User_level == "Administrador") {
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [

                    ['label' => 'Mapa', 'url' => ['site/index']
                    ],

                    ['label' => 'Servicio', 'items' => [
                        ['label' => 'Instalación/Venta', 'url' => ['servicio/createinstalacionventa']],
                        ['label' => 'Mantencion', 'url' => ['servicio/createmantencion']],
                        ['label' => 'Renovar Garantía', 'url' => ['servicio/createrenovaciongarantia']],
                        ['label' => 'Calendario', 'url' => ['servicio/calendario']],
                    ]
                    ],

                    ['label' => 'Pago', 'url' => ['pago/createpago']
                    ],

                    ['label' => 'Mantenedores', 'items' => [
                        ['label' => 'Cliente', 'url' => ['cliente/index']],
                        ['label' => 'Técnico', 'url' => ['tecnico/index']],
                        ['label' => 'Producto', 'url' => ['producto/index']],
                        ['label' => 'Proveedor', 'url' => ['proveedor/index']],
                        ['label' => 'Usuarios', 'url' => ['usuarios/index']],
                    ]
                    ],

                    ['label' => 'Informes', 'items' => [
                        ['label' => 'Servicios por Técnico', 'url' => ['tecnico/servicioportecnico']],
                        ['label' => 'Servicios Periódicos', 'url' => ['servicio/servicioperiodico']],
                    ]
                    ],
                    Yii::$app->user->isGuest ? (
                    ['label' => 'Login', 'url' => ['/site/login']]
                    ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Logout (' . Yii::$app->user->identity->Username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>'
                    )
                ],
            ]);
        } else {
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [

                    ['label' => 'Servicio', 'items' => [
                        ['label' => 'Instalacion/Venta', 'url' => ['servicio/createinstalacionventa']],
                        ['label' => 'Mantencion', 'url' => ['servicio/createmantencion']],
                        ['label' => 'Renovar Garantía', 'url' => ['servicio/createrenovaciongarantia']],
                        ['label' => 'Calendario', 'url' => ['servicio/calendario']],
                    ]
                    ],
                    ['label' => 'Pago', 'url' => ['pago/createpago']
                    ],
                    ['label' => 'Mantenedores', 'items' => [
                        ['label' => 'Cliente', 'url' => ['cliente/index']],
                        ['label' => 'Tecnico', 'url' => ['tecnico/index']],
                        ['label' => 'Producto', 'url' => ['producto/index']],
                        ['label' => 'Proveedor', 'url' => ['proveedor/index']],
                    ]
                    ],

                    Yii::$app->user->isGuest ? (
                    ['label' => 'Login', 'url' => ['/site/login']]
                    ) : (
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'Logout (' . Yii::$app->user->identity->Username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>'
                    )
                ],
            ]);
        }
    }




    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>

    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Mayar Ltda <?= date('Y') ?></p>


        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
