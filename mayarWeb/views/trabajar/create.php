<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Trabajar */

$this->title = 'Create Trabajar';
$this->params['breadcrumbs'][] = ['label' => 'Trabajars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trabajar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
