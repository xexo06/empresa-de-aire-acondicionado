<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Tecnico;
use app\models\Servicio;

/* @var $this yii\web\View */
/* @var $model app\models\Trabajar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trabajar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, 'FK_tecnico')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Tecnico::find()->all(),'RUT','nombre'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecciona una opción'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?php
    echo $form->field($model, 'FK_servicio')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Servicio::find()->all(),'nFactura','nFactura'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecciona una opción'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
