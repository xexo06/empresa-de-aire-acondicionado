<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Garantia */

$this->title = 'Update Garantia: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Garantias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nGarantia, 'url' => ['view', 'id' => $model->nGarantia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="garantia-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
