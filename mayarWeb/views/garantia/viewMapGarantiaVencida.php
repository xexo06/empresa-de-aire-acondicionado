<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Garantia */

$this->title = $model->nGarantia;
$this->params['breadcrumbs'][] = ['label' => 'Garantias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="garantia-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->nGarantia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->nGarantia], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <h1> Garantia</h1>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nGarantia',
            'fechaFin',
            'estado',
            'FK_equipo',
        ],
    ]) ?>

    <?php
    $producto= $_GET["producto"];

    ?>
    <?php
    $sql1 = "SELECT * FROM equipo e WHERE e.nSerie=".$producto." ";
    $modelsEquipo= \app\models\Equipo::findBySql($sql1)->one();

    ?>
    <h1> Equipo</h1>
    <?= DetailView::widget([
        'model' => $modelsEquipo,
        'attributes' => [
            'nSerie',
            'FK_cliente',
            'FK_producto',
            'lng:ntext',
            'lat:ntext',
        ],
    ]) ?>


    <?php
    $cliente= $_GET["cliente"];

    ?>
    <?php
    $sql1 = "SELECT * FROM cliente c WHERE c.RUT='".$cliente."' ";
    $modelsCliente= \app\models\Cliente::findBySql($sql1)->one();

    ?>
    <h1> Cliente</h1>
    <?= DetailView::widget([
        'model' => $modelsCliente,
        'attributes' => [
            'RUT',
            'correo',
            'ciudad',
            'nombre',
            'telefono',
            'direccion',
        ],
    ]) ?>

</div>