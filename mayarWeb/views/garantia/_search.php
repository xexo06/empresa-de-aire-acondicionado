<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GarantiaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="garantia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nGarantia') ?>

    <?= $form->field($model, 'fechaFin') ?>

    <?= $form->field($model, 'estado') ?>

    <?= $form->field($model, 'FK_equipo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
