<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datetimepicker\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Equipo;


/* @var $this yii\web\View */
/* @var $model app\models\Garantia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="garantia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nGarantia')->textInput(['maxlength' => true]) ?>

    <?php
    echo '<label>fechaFin</label>';
    echo DateTimePicker::widget([
        'model' => $model,
        'attribute' => 'fechaFin',
        'language' => 'es',
        'size' => 'ms',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd P',
            'todayBtn' => true
        ]
    ]);?>

    <?= $form->field($model, 'estado')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'FK_equipo')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Equipo::find()->all(),'nSerie','nSerie'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecciona una opción'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
