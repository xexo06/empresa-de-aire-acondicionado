<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use wbraganca\dynamicform\DynamicFormWidget;
use app\models\servicio;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Servicio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servicio-form panel-guion">
    <?php $form = ActiveForm::begin(['id'=>'dynamic-form','action' => 'index.php?r=pago%2Fcreatepago']); ?>

    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading"><h4><i class="glyphicon glyphicon-menu-hamburger"></i> Agregar Pago</h4></div>
                <div class="panel-body">
                    <div class="col-md-6">
                        <?php
                        echo $form->field($model, 'FK_servicio')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map($modelServicio,'nFactura','nFactura'),
                            'language' => 'es',
                            'options' => ['placeholder' => 'Selecciona una opción'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'interes')->textInput(['type' => 'number', 'min' => 0]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'valorServicio')->textInput(['type' => 'number', 'min' => 0]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'valorEquipo')->textInput(['type' => 'number', 'min' => 0]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading"><h4><i class="glyphicon glyphicon-menu-hamburger"></i> Agregar Cuotas</h4></div>
                <div class="panel-body">
                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "" [A-Za-z0-9]
                        'widgetBody' => '.container-items', // required: css class selector
                        'widgetItem' => '.item', // required: css cla
                        'limit' => 4,
                        'min' => 1, // 0 or 1 (default 1)
                        'insertButton' => '.add-item', // css class
                        'deleteButton' => '.remove-item', // css class
                        'model' => $modelsCuotas[0],
                        'formId' => 'dynamic-form',
                        'formFields' => [
                            'fechaRecibo',
                            'fechaPago',
                            'estado',
                            'valor',
                            'tipo',
                        ],
                    ]); ?>

                    <div class="container-items"><!-- widgetContainer -->
                        <?php foreach ($modelsCuotas as $i => $modelCuotas): ?>
                            <div class="item panel panel-success"><!-- widgetBody -->
                                <div class="panel-heading">
                                    <h3 class="panel-title pull-left">Cuota</h3>
                                    <div class="pull-right">
                                        <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                        <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <?php
                                    // necessary for update action.
                                    /* if (! $modelProducto->isNewRecord) {
                                         echo Html::activeHiddenInput($modelProducto, "[{$i}]numeroSerie");
                                     }*/
                                    ?>
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php
                                                echo $form->field($modelCuotas, "[{$i}]fechaRecibo")->widget(DatePicker::className(), [
                                                    // inline too, not bad
                                                    'inline' => true,
                                                    // modify template for custom rendering
                                                    'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                                                    'language'=>'es',
                                                    'clientOptions' => [
                                                        'autoclose' => true,
                                                        'format' => 'yyyy-mm-dd'
                                                    ]
                                                ]);?>
                                            </div>
                                            <div class="col-md-6">
                                                <?php
                                                echo $form->field($modelCuotas, "[{$i}]fechaPago")->widget(DatePicker::className(), [
                                                    // inline too, not bad
                                                    'inline' => true,
                                                    // modify template for custom rendering
                                                    'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                                                    'language'=>'es',
                                                    'clientOptions' => [
                                                        'autoclose' => true,
                                                        'format' => 'yyyy-mm-dd'
                                                    ]
                                                ]);?>
                                            </div>
                                            <div class="col-md-4">
                                                <?= $form->field($modelCuotas, "[{$i}]estado")->textInput(['maxlength' => true])->dropDownList(['pendiente' => 'pendiente','pagado' => 'pagado']) ?>
                                            </div>
                                            <div class="col-md-4">
                                                <?= $form->field($modelCuotas, "[{$i}]valor")->textInput(['type' => 'number', 'min' => 1]) ?>
                                            </div>
                                            <div class="col-md-4">
                                                <?= $form->field($modelCuotas, "[{$i}]tipo")->textInput(['maxlength' => true])->dropDownList(['cheque' => 'cheque','efectivo' => 'efectivo','transferencia'=>'transferencia','credito' => 'credito','debito' => 'debito']) ?>
                                            </div>


                                        </div><!-- .row -->
                                    </div>

                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <?php DynamicFormWidget::end(); ?>
                </div>
            </div>
        </div>

    </div>
    <div class="form-group col-lg-offset-11">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success btn-block btn-lg']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>

<script>

        $(".dynamicform_wrapper").on("afterInsert afterDelete", function(e, item) {
            var VarItemsQuantity = $('.item').length,
                VarCounter = 1;
            for(VarCounter; VarCounter < VarItemsQuantity; VarCounter++) {
                $('#cuotas-' + VarCounter + '-fecharecibo').
                parent().datepicker({"autoclose":true,  "format":"yyyy-mm-dd", 'language':'es',});
                $('#cuotas-' + VarCounter + '-fechapago').
                parent().datepicker({"autoclose":true,  "format":"yyyy-mm-dd", 'language':'es',});
            };
        });

</script>