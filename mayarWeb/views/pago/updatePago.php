<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pago */

$this->title = 'Actualizar Pago, Factura: '.$model->FK_servicio;
$this->params['breadcrumbs'][] = ['label' => 'Pagos', 'url' => ['createpago']];
$this->params['breadcrumbs'][] = ['label' => 'Factura '.$model->FK_servicio, 'url' => ['servicio/viewInstalacionVenta', 'id' => $model->FK_servicio]];
$this->params['breadcrumbs'][] = 'Actualizar Pago';
?>
<div class="pago-update contenido">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formPago', [
        'model' => $model,
        'modelsCuotas' => $modelsCuotas,
        'modelServicio' => $modelServicio,
    ]) ?>

</div>