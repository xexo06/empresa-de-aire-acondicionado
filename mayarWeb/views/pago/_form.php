<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\servicio;

/* @var $this yii\web\View */
/* @var $model app\models\Pago */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pago-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'numeroCuotas')->textInput() ?>

    <?= $form->field($model, 'valorServicio')->textInput() ?>

    <?= $form->field($model, 'valorEquipo')->textInput() ?>

    <?= $form->field($model, 'interes')->textInput() ?>

    <?php
    echo $form->field($model, 'FK_servicio')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Servicio::find()->all(),'nFactura','nFactura'),
        'language' => 'es',
        'options' => ['placeholder' => 'Selecciona una opción'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
