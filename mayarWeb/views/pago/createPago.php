<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model app\models\Pago */

$this->title = 'Crear Pago';
$this->params['breadcrumbs'][] = "Crear Pago";
?>

<!--
<form action="index.php" method="post">
    Nombre: <input type="text" name="nombre"><br>
    Email: <input type="text" name="email"><br>
    Enviar <input type="submit" name="enviar">
</form> -->

<div class="pago-create contenido">
    <div class="row">
        <div class="col-md-6">
            <h1 style="margin-top:0px">Crear Pago</h1>
        </div>
        <div class="col-md-6">
            <div class="pull-right">
                <a class="btn btn-warning" href="index.php?r=servicio%2Fcreateinstalacionventa" role="button">Crear Instalación/Venta</a>
                <a class="btn btn-info" href="index.php?r=servicio%2Fcreatemantencion" role="button">Crear Mantención</a>
                <a class="btn btn-success" href="index.php?r=servicio%2Fcreaterenovaciongarantia" role="button">Renovar Garantía</a>
            </div>
        </div>
    </div>
    <?= $this->render('_formPago', [
        'model' => $model,
        'modelsCuotas' => $modelsCuotas,
        'modelServicio' => $modelServicio,
    ]) ?>

</div>
<br>
<div class="row">
    <div class="col-md-12">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'ID',
                'valorServicio',
                'valorEquipo',
                'interes',
                'FK_servicio',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Acciones',
                    'template' => '{view}{update}{delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver'),
                            ]);
                        },

                        'update' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Actualizar'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'Eliminar'),
                                'class' => 'eliminar',
                                'data' => [
                                    'confirm' => '¿Está seguro que desea eliminar este pago?'
                                ]
                            ]);
                        }
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url ='index.php?r=pago%2Fviewinstalacionventa&id='.$model->FK_servicio;
                            return $url;
                        }
                        if ($action === 'update') {
                            $url ='index.php?r=pago%2Fupdatepago&id='.$model->FK_servicio;
                            return $url;
                        }
                        if ($action === 'delete') {
                            $url ='index.php?r=pago%2Fdeletepago&id='.$model->FK_servicio;
                            return $url;
                        }

                    }
                ],
            ],
        ]); ?>
    </div>
</div>

