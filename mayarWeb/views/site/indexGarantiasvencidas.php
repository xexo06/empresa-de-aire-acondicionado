<?php
use yii\helpers\Html;
use app\models\Cliente;
use app\models\Garantia;
use app\models\Producto;


$this->title = 'Mayar Ltda';
?>


<p>
    <?= Html::a('Todos', ['index'], ['class' => 'btn btn-default']) ?>
    <?= Html::a('Morosos', ['mapmorosos'], ['class' => 'btn btn-danger']) ?>
    <?= Html::a('Garantias Vencidas', ['mapgarantiasvencidas'], ['class' => 'btn btn-warning']) ?>

</p>
<div class="site-index">

    <div class="body-content">

        <?php
        /*foreach ($dataProvider->models as $model) {
            $prueba= $model['RUT_cliente'];
            echo $prueba;

        }
         foreach($cliente as $data){
            echo $data['nombre'];
         }

        foreach ($dataProvider->models as $model){
            echo '---'.$model['RUT_cliente'].'--';

            foreach($cliente as $data){
                if($model['RUT_cliente']==$data['RUT']){

                    echo $model['RUT_cliente'];
                    echo $data['RUT'];
                }
            }
        }
     */
        ?>
        <div class="row">

            <div class="col-md-12">
                <div id="map" style=" height: 500px; width: 100%;">

                </div>
            </div>
        </div>

    </div>
</div>
<script>

    function initMap() {



        var myLatLng = {lat: -36.423451, lng: -71.960154};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: myLatLng
        });

        <?php
        foreach ($equipo as $e){
            $consultaCliente = "select * from cliente join equipo on RUT = FK_cliente where nSerie = " . $e['nSerie'] . " ";
            $cliente = Cliente::findBySql($consultaCliente)->one();

            $consultaProducto = "select * from producto where ID = '" . $e['FK_producto'] . "' ";
            $producto = Producto::findBySql($consultaProducto)->one();

            $consultaGarantia = "select * from garantia where estado = 'si' AND FK_equipo = '" . $e['nSerie'] . "' ";
            $garantia = Garantia::findBySql($consultaGarantia)->one();

            $consultaCount = "select * from garantia where FK_equipo = '" . $e['nSerie'] . "' ";
            $count = Garantia::findBySql($consultaCount)->count();
            ?>
            var marker = new google.maps.Marker({
                position: {
                    lat: <?php echo $e['lat']; ?>,
                    lng: <?php echo $e['lng']; ?>
                },
                map: map,
                icon: 'css/Vencidas.png'
            });
            var nombre = "<div class='row'>" +
            "   <div class='col-md-12'>" +
            "       <div class='col-md-6'>" +
            "           <div class='panel panel-success'>" +
            "               <div class='panel-heading'>" +
            "                   Cliente" +
            "               </div>" +
            "               <div class='panel-body'>" +
            "                 <p><strong>Nombre:</strong> <?php echo $cliente['nombre'];?> </p> " +
            "                 <p><strong>Teléfono:</strong> <?php echo $cliente['telefono'];?></p>" +
            "                 <p><strong>Dirección:</strong> <?php echo $cliente['direccion'];?></p>" +
            "                 <p><strong>Correo:</strong> <?php echo $cliente['correo'];?></p>"+
            "               </div>" +
            "           </div>" +
            "       </div>" +
            "       <div class='col-md-6'>" +
            "           <div class='panel panel-info'>" +
            "               <div class='panel-heading'>" +
            "                   Equipo" +
            "               </div>" +
            "               <div class='panel-body'>" +
            "                   <p><strong>Número de Serie:</strong>   <?php echo $e['nSerie'];?></p> " +
            "                   <p><strong>Refrigerante:</strong>   <?php echo $producto['refrigerante'];?></p>" +
            "                   <p><strong>Modelo:</strong>   <?php echo $producto['modelo'];?></p>" +
            "                   <p><strong>Tipo:</strong>   <?php echo $producto['tipo'];?></p>" +
            "               </div>" +
            "           </div>" +
            "       </div>" +
            "   </div>" +
            "   <div class='col-md-12'>" +
            "       <div class='col-md-6'>" +
            "           <div class='panel panel-warning'>" +
            "               <div class='panel-heading'>" +
            "                   Garantía" +
            "               </div>" +
            "               <div class='panel-body'>" +
            "                   <p><strong>Garantía: </strong> <?php echo $count;?></p> " +
            "                   <p><strong>Fecha Vencimiento: </strong> <?php echo $garantia['fechaFin'];?></p> " +
            "               </div>" +
            "           </div>" +
            "       </div>" +
            "   </div>" +
            "</div>";

            attachSecretMessage(marker, nombre);
            <?php
        }
        ?>


        function attachSecretMessage(marker, secretMessage) {
            var infowindow = new google.maps.InfoWindow({
                content: secretMessage
            });
            marker.addListener('click', function () {
                infowindow.open(marker.get('map'), marker);
            });
        }
    }




</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2JRFa_y6AbGzJudjmJabe_NkFgt74cao&callback=initMap">
    src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap">
</script>