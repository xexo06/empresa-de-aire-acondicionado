<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;

use app\models\Cliente;
use app\models\Producto;
use app\models\Tecnico;
use app\models\Servicio;




$this->title = 'Mayar Ltda';
if (Yii::$app->user->identity->User_level == "Administrador"){
?>


<p>
    <?= Html::a('Todos', ['index'], ['class' => 'btn btn-default']) ?>
    <?= Html::a('Morosos', ['mapmorosos'], ['class' => 'btn btn-danger']) ?>
    <?= Html::a('Garantias Vencidas', ['mapgarantiasvencidas'], ['class' => 'btn btn-warning']) ?>




</p>
<div class="site-index">

    <div class="body-content">

        <?php
        /*foreach ($dataProvider->models as $model) {
            $prueba= $model['RUT_cliente'];
            echo $prueba;

        }
         foreach($cliente as $data){
            echo $data['nombre'];
         }*/

        /* foreach ($dataProvider->models as $model){
             echo '---'.$model['RUT_cliente'].'--';

             foreach($cliente as $data){
                 if($model['RUT_cliente']==$data['RUT']){

                     echo $model['RUT_cliente'];
                     echo $data['RUT'];
                 }
             }
         }

       */
        ?>
        <div class="row">

            <div class="col-md-12">
                <div id="map" style=" height: 500px; width: 100%;">

                </div>
            </div>
        </div>

    </div>
</div>

<script>

    function initMap() {



        var myLatLng = {lat: -36.423451, lng: -71.960154};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: myLatLng
        });



        // To add the marker to the map, call setMap();



        <?php
        foreach ($equipo as $e){
            $consultaCliente = "select * from cliente join equipo on RUT = FK_cliente where nSerie = " . $e['nSerie'] . " ";
            $cliente = Cliente::findBySql($consultaCliente)->one();

            $consultaProducto = "select * from producto where ID = '" . $e['FK_producto'] . "' ";
            $producto = Producto::findBySql($consultaProducto)->one();

            $consultaServicio = "select * from servicio where nFactura in (SELECT FK_servicio from tener where FK_equipo in (select nSerie from equipo where nSerie = '".$e['nSerie']."'))";
            $servicio = Servicio::findBySql($consultaServicio)->one();

            $consultaTecnico = "select * from tecnico where RUT in (select FK_tecnico from trabajar where FK_servicio in (select nFactura from servicio where nFactura in (SELECT FK_servicio from tener where FK_equipo in (select nSerie from equipo where nSerie = '".$e['nSerie']."'))))";
            $tecnico = Tecnico::findBySql($consultaTecnico)->all();
            ?>

            var marker = new google.maps.Marker({
                position: {
                    lat: <?php echo $e['lat']; ?>,
                    lng: <?php echo $e['lng']; ?>
                },
                map: map,
                icon: 'css/Todos.png'
            });
        <?php
        $fecha = date_create($servicio['fecha']);
        $fechaFormateada =  date_format($fecha, 'Y-m-d h:i');
        ?>
            var nombre = "<div class='row'>" +
                "   <div class='col-md-12'>" +
                "       <div class='col-md-6'>" +
                "           <div class='panel panel-success'>" +
                "               <div class='panel-heading'>" +
                "                   Cliente" +
                "               </div>" +
                "               <div class='panel-body'>" +
                "                 <p><strong>Nombre:</strong> <?php echo $cliente['nombre']?> </p> " +
                "                 <p><strong>Teléfono:</strong> <?php echo $cliente['telefono']?></p>" +
                "                 <p><strong>Dirección:</strong> <?php echo $cliente['direccion']?></p>" +
                "                 <p><strong>Correo:</strong> <?php echo $cliente['correo']?></p>" +
                "               </div>" +
                "           </div>" +
                "       </div>" +
                "       <div class='col-md-6'>" +
                "           <div class='panel panel-info'>" +
                "               <div class='panel-heading'>" +
                "                   Equipo" +
                "               </div>" +
                "               <div class='panel-body'>" +
                "                   <p><strong>Serie:</strong>   <?php echo $e['nSerie']?></p> " +
                "                   <p><strong>Refrigerante:</strong>   <?php echo $producto['refrigerante']?></p>" +
                "                   <p><strong>Modelo:</strong>   <?php echo $producto['modelo']?></p>" +
                "                   <p><strong>Tipo:</strong>   <?php echo $producto['tipo']?></p>" +
                "               </div>" +
                "           </div>" +
                "       </div>" +
                "   </div>" +
                "   <div class='col-md-12'>" +
                "       <div class='col-md-6'>" +
                "           <div class='panel panel-warning'>" +
                "               <div class='panel-heading'>" +
                "                   Factura" +
                "               </div>" +
                "               <div class='panel-body'>" +
                "                   <p><strong>Factura: </strong> <?php echo $servicio['nFactura']?></p> " +
                "                   <p><strong>Técnico(s): </strong> <?php foreach ($tecnico as $tec){ echo $tec['nombre']." ";}?>"+
                "                   <p><strong>Fecha Instalación: </strong><?php echo $fechaFormateada;?></p> " +
                "               </div>" +
                "           </div>" +
                "       </div>" +
                "   </div>" +
                "</div>";
            attachSecretMessage(marker, nombre);
            <?php
        }
        ?>
        function attachSecretMessage(marker, secretMessage) {
            var infowindow = new google.maps.InfoWindow({
                content: secretMessage
            });
            marker.addListener('click', function () {
                infowindow.open(marker.get('map'), marker);
            });
        }
    }




</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2JRFa_y6AbGzJudjmJabe_NkFgt74cao&callback=initMap">
    src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap">
</script>
<?php }else{
    Yii::$app->response->redirect(yii\helpers\Url::to(['servicio/createinstalacionventa'], true));

} ?>