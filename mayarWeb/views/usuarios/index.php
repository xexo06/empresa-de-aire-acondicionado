<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuarios-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Crear Usuario', ['id' => 'modelButtonusuarios', 'value' =>Url::to('index.php?r=usuarios%2Fcreateajax'), 'class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Id',
            'Rut',
            'First_name',
            'Last_name',
            'Telefono',
            //'Username',
            //'Email:email',
            //'Password',
            //'authKey',
            //'User_level',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
<?php
Modal::begin([
    'header' => '<h3 class="tituloModal" align="center">Ingresar Usuario</h3>',
    'id'     => 'modal5',
    'size'   => 'modal-md',
]);

echo "<div id='modelContent5'></div>";

Modal::end();
?>