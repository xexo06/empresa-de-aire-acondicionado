<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuarios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuarios-form">

    <?php $form = ActiveForm::begin(['action' => 'index.php?r=usuarios%2Fcreateajax', 'id' => $model->formName()]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'Rut')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'First_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Last_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Telefono')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Username')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Password')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'authKey')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'User_level')->dropDownList(['Cliente' => 'Cliente', 'Administrador' => 'Administrador']) ?>
        </div>
    </div>
    <div class="row">
        <hr>
        <div class="col-md-12">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success col-md-offset-10']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
//beforeSubmit
$js = "
        $('form#".$model->formName()."').on('beforeSubmit', function(e){
            var \$form = $(this);
            if( Rut($('#usuarios-rut').val())){
                submitMySecondForm(\$form);
            }
        }).on('submit', function(e){
            e.preventDefault();
        });";
$this->registerJs($js);
?>
