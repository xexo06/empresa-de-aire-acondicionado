<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Producto */

?>
<div class="producto-create">

    <?= $this->render('_formajax', [
        'model' => $model,
    ]) ?>

</div>
