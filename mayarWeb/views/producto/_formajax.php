<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Proveedor;

/* @var $this yii\web\View */
/* @var $model app\models\Producto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="producto-form">

    <?php $form = ActiveForm::begin(['action' => 'index.php?r=producto%2Fcreateajax', 'id' => $model->formName()]); ?>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'stock')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'tipo')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'capacidad')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'modelo')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'refrigerante')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?php
            echo $form->field($model, 'FK_proveedor')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Proveedor::find()->all(),'RUT','nombre'),
                'language' => 'es',
                'options' => ['placeholder' => 'Selecciona una opción'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <hr>
        <div class="col-md-12">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success col-md-offset-10']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
<?php
//beforeSubmit
$js = "
        $('form#".$model->formName()."').on('beforeSubmit', function(e){
            var \$form = $(this);
            submitMySecondForm(\$form);
        }).on('submit', function(e){
            e.preventDefault();
        });";
$this->registerJs($js);
?>