<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\helpers\Url;
use yii\bootstrap\Modal;

use app\models\Producto;
use app\models\Equipo;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Crear Producto', ['id' => 'modelButtonproducto', 'value' =>Url::to('index.php?r=producto%2Fcreateajax'), 'class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'stock',
            'capacidad',
            'modelo',
            'tipo',
            //'FK_proveedor',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'Ver'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Actualizar'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Eliminar'),
                            'class' => 'eliminar',
                            'data' => [
                                'confirm' => '¿Está seguro que desea eliminar este producto?'
                            ]
                        ]);
                    }
                ],


                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=producto%2Fview&id='.$model->ID;
                        return $url;
                    }
                    if ($action === 'update') {
                        $url ='index.php?r=producto%2Fupdate&id='.$model->ID;
                        return $url;
                    }
                    if ($action === 'delete') {

                        $sql = "SELECT * FROM equipo WHERE FK_producto='".$model->ID."' ";
                        $Equipos =Equipo::findBySql($sql)->count();


                        if($Equipos>0) {
                            $url = 'index.php?r=producto%2Fdelete&id=1';
                            return $url;

                        }else{
                            $url = 'index.php?r=producto%2Fdelete&id='.$model->ID;
                            return $url;
                        }

                    }

                }



            ],
            ],

    ]); ?>
</div>

<?php
Modal::begin([
'header' => '<h3 class="tituloModal" align="center">Ingresar producto</h3>',
'id'     => 'modal3',
'size'   => 'modal-md',
]);

echo "<div id='modelContent3'></div>";

Modal::end();
?>