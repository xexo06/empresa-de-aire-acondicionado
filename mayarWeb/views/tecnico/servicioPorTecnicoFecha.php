<?php
/**
 * Created by PhpStorm.
 * User: GEST
 * Date: 24-06-2018
 * Time: 23:03
 */
use yii\helpers\Html;
use dosamigos\datetimepicker\DateTimePicker;
use app\models\Trabajar;
use yii\widgets\ActiveForm;
$this->title = 'Servicios por Técnico';
$this->params['breadcrumbs'][] = ['label' => 'Tecnicos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tecnico-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <?php $form = ActiveForm::begin(['action' => 'index.php?r=tecnico%2Fservicioportecnicofecha', 'method' => 'GET']); ?>
        <div class="col-md-3">

            <?php

            echo '<label>Fecha</label>';
            echo DateTimePicker::widget([
                'name' => 'fecha1',
                'attribute' => 'fecha1',
                'options' => ['placeholder' => 'Fecha Inicio'],
                'language' => 'es',
                'size' => 'ms',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd HH:ii:ss P',
                    'todayBtn' => true
                ]
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <?php
            echo '<label>Fecha</label>';
            echo DateTimePicker::widget([
                'name' => 'fecha2',
                'attribute' => 'fecha2',
                'options' => ['placeholder' => 'Fecha Fin'],
                'language' => 'es',
                'size' => 'ms',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd HH:ii:ss P',
                    'todayBtn' => true
                ]
            ]);
            ?>
        </div>
        <div class="col-md-3" style="padding-top :25px">
            <?= Html::submitButton('Filtrar', ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        <div class="col-md-4">

        </div>
    </div>
    <br>
    <hr>
    <br>
    <div class="row">
        <?php
        foreach ($model as $tec){
            ?>
            <div class="col-md-4">
                <ul class="list-group">
                    <li class="list-group-item active"><?php echo $tec['nombre'];?></li>
                    <?php
                    foreach ($modelTiposervicio as $ts){
                        if($ts['tipo'] == 'Instalación' || $ts['tipo'] == 'Venta' || $ts['tipo'] == 'Mantención'){
                            $consulta="SELECT * FROM (servicio s join trabajar t on s.nFactura = t.FK_servicio) join tecnico tec on t.FK_tecnico = tec.RUT where s.fecha between '".$fechaInicio."' AND '".$fechaFin."' AND s.FK_tiposervicio =".$ts['ID']." and tec.RUT = '".$tec['RUT']."'";
                            $count = Trabajar::findBySql($consulta)->count();
                            ?>
                            <li class="list-group-item">
                                <span class="badge"><?php echo $count; ?></span>
                                <?php echo $ts['tipo']?>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
            <?php
        }
        ?>

    </div>
</div>
<script>
    $(document).ready(function(){
        $("#w1").val('<?php echo $fechaInicio;?>');
        $("#w2").val('<?php echo $fechaFin;?>');
    });

</script>