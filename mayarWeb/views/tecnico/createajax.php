<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tecnico */

?>
<div class="tecnico-create">

    <?= $this->render('_formajax', [
        'model' => $model,
    ]) ?>

</div>
