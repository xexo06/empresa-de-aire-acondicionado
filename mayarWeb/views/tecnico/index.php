<?php

use yii\helpers\Html;
use yii\grid\GridView;

use yii\helpers\Url;
use yii\bootstrap\Modal;

use app\models\Tecnico;
use app\models\Trabajar;



/* @var $this yii\web\View */
/* @var $searchModel app\models\TecnicoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tecnicos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tecnico-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::button('Crear Técnico', ['id' => 'modelButtontecnico', 'value' =>Url::to('index.php?r=tecnico%2Fcreateajax'), 'class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'RUT',
            'correo',
            'telefono',
            'direccion',
            'nombre',

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'Ver'),
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => Yii::t('app', 'Actualizar'),
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('app', 'Eliminar'),
                            'class' => 'eliminar',
                            'data' => [
                                'confirm' => '¿Está seguro que desea eliminar este tecnico?'
                            ]
                        ]);
                    }
                ],
                //index.php?r=tecnico%2Fview&id=10686316-4
                ///index.php?r=tecnico%2Fupdate&id=10686316-4
                //index.php?r=tecnico%2Fdelete&id=10686316-4

                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='index.php?r=tecnico%2Fview&id='.$model->RUT;
                        return $url;
                    }
                    if ($action === 'update') {
                        $url ='index.php?r=tecnico%2Fupdate&id='.$model->RUT;
                        return $url;
                    }
                    if ($action === 'delete') {

                        $sql = "SELECT * FROM trabajar WHERE FK_tecnico='".$model->RUT."' ";
                        $Trabajos =Trabajar::findBySql($sql)->count();


                        if($Trabajos>0) {
                            $url = 'index.php?r=tecnico%2Fdelete&id=1';
                            return $url;

                        }else{
                            $url = 'index.php?r=tecnico%2Fdelete&id='.$model->RUT;
                            return $url;
                        }

                    }

                }



            ],
        ],
    ]); ?>
</div>

<?php
Modal::begin([
'header' => '<h3 class="tituloModal" align="center">Ingresar Técnico</h3>',
'id'     => 'modal2',
'size'   => 'modal-md',
]);

echo "<div id='modelContent2'></div>";

Modal::end();
?>