<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\EquipoSearch;
use app\models\Cliente;
use app\models\Pago;
use app\models\Cuotas;
use app\models\Tener;
use app\models\Servicio;
use app\models\Equipo;
use app\models\Garantia;
use app\models\Producto;
use app\models\Usuarios;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' =>[
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','mapmorosos','mapgarantiasvencidas','login','logoup','contact','about'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' =>['index','mapmorosos','mapgarantiasvencidas','login','logout','contact','about'],
                        'roles'=>['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Usuarios::isUserAdministrador(Yii::$app->user->identity->Username);
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' =>['index','mapmorosos','mapgarantiasvencidas','login','logout','contact','about'],
                        'roles'=>['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Usuarios::isUserSecretaria(Yii::$app->user->identity->Username);
                        }
                    ],
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $actualizar= Equipo::find()->all();
        foreach($actualizar as $actualizar1) {
            $fechaActual = date('Y-m-d');
            $sql = "SELECT * FROM garantia g WHERE g.FK_equipo=".$actualizar1['nSerie']." ";
            $count = count(Garantia::findBySql($sql)->all());
            $sql1 = "SELECT * FROM garantia g WHERE g.FK_equipo=".$actualizar1['nSerie']." and estado='si' and fechaFin<='". $fechaActual ."'";
            $siono = Garantia::findBySql($sql1)->one();
            if ($count == 3 && $siono != null) {
                $sql5 = "SELECT * FROM garantia g WHERE FK_equipo=".$actualizar1['nSerie']." and g.estado='si' and g.fechaFin<='".$fechaActual."' ";
                $Garantia = Garantia::findBySql($sql5)->one();
                $id = $Garantia['nGarantia'];
                $Garantia1 = Garantia::findOne($id);
                $estado = 'no';
                $Garantia1->estado = $estado;
                $Garantia1->update();
            }
        }

        $consultaEquipo = "Select * from equipo where nSerie in (select FK_equipo from tener where FK_servicio in (select nFactura from servicio where estado = 'realizado' AND (FK_tipoServicio = 1 or FK_tipoServicio = 2)))";
        $equipo=Equipo::findBySql($consultaEquipo)->all();
        if($equipo == null){
            Yii::$app->session->setFlash('danger','¡No hay equipos de AA instalados!');
            return $this->render('index', [
                'equipo'=>$equipo,
            ]);
        }
        return $this->render('index', [
            'equipo'=>$equipo,
        ]);
    }



    public function actionMapmorosos()
    {
        $consultaEquipo = "select * from equipo where nSerie in (select FK_equipo from tener where FK_servicio in (SELECT FK_servicio from pago where ID in (select FK_pago from cuotas where estado = 'pendiente' and fechaRecibo<now())))";
        $equipo = Equipo::findBySql($consultaEquipo)->all();
        if($equipo == null){
            Yii::$app->session->setFlash(
                'danger','¡No hay equipos de AA con una deuda asociada'
            );
            return $this->render('indexMorosos', [
                'equipo'=>$equipo,
            ]);
        }
        return $this->render('indexMorosos', [
            'equipo'=>$equipo,
        ]);
    }


    public function actionMapgarantiasvencidas()
    {
        $consultaEquipo = "select * from equipo join garantia on nSerie = FK_equipo where estado = 'si' and fechaFin < now()";
        $equipo = Equipo::findBySql($consultaEquipo)->all();
        if($equipo == null) {
            Yii::$app->session->setFlash(
                'danger', '¡No hay equipos con garantias vencidas'
            );
            return $this->render('indexMorosos', [
                'equipo' => $equipo,
            ]);
        }
        return $this->render('indexGarantiasvencidas', [
            'equipo' => $equipo,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}

