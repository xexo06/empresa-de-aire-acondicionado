<?php

namespace app\controllers;

use app\models\Trabajar;
use Yii;
use app\models\Tecnico;
use app\models\Tiposervicio;
use app\models\TecnicoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

use app\models\Servicio;
use app\models\Usuarios;

/**
 * TecnicoController implements the CRUD actions for Tecnico model.
 */
class TecnicoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete' => ['GET'],
                ],
            ],
            'access' =>[
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','view','create','createajax','update','delete','servicioportecnico','servicioportecnicofecha'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions'=>['index','view','create','createajax','update','delete','servicioportecnico','servicioportecnicofecha'],
                        'roles' =>['@'],
                        'matchCallback'=> function ($rule, $action){
                            return Usuarios::isUserAdministrador(Yii::$app->user->identity->Username);
                        }
                    ],

                    [
                        'allow' => true,
                        'actions'=>['index','view','create','createajax','update','delete'],
                        'roles' =>['@'],
                        'matchCallback'=> function ($rule, $action){
                            return Usuarios::isUserSecretaria(Yii::$app->user->identity->Username);
                        }
                    ],

                ],
            ],
        ];
    }

    /**
     * Lists all Tecnico models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TecnicoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tecnico model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tecnico model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tecnico();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->RUT]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateajax()
    {
        $model = new Tecnico();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->refresh();
            Yii::$app->response->format = 'json';
            return ['message' => Yii::t('app','¡El Técnico ha sido creado con éxito!'), 'id'=>$model->RUT];
        }
        return $this->renderAjax('createajax', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tecnico model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash(
                'success','¡Técnico actualizado con éxito!'
            );
            return $this->redirect(['view', 'id' => $model->RUT]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tecnico model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        if($id==1) {
            Yii::$app->session->setFlash(
                'danger', '¡El Tecnico no se puede eliminar !'
            );
            return $this->redirect(['index']);//controlador
            // render vista

        }

        $this->findModel($id)->delete();
        Yii::$app->session->setFlash(
            'success','¡Técnico eliminado con éxito!'
        );
        return $this->redirect(['index']);
    }



    /**
     * Finds the Tecnico model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tecnico the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tecnico::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionServicioportecnico(){
        $model = Tecnico::find()->all();
        $modelTiposervicio = Tiposervicio::find()->all();
        return $this->render('servicioPorTecnico', [
            'model' => $model,
            'modelTiposervicio' => $modelTiposervicio,
        ]);
    }
    public function actionServicioportecnicofecha(){
        $fecha1 = $_GET['fecha1'];
        $fecha2 = $_GET['fecha2'];
        $model = Tecnico::find()->all();
        $modelTiposervicio = Tiposervicio::find()->all();
        if(strlen($fecha1) == 0 && strlen($fecha2) == 0){
            Yii::$app->session->setFlash(
                'danger','¡Error! Debe ingresar las fechas'
            );
            return $this->render('servicioPorTecnico', [
                'model' => $model,
                'modelTiposervicio' => $modelTiposervicio,
            ]);
        }else{
            if(strlen($fecha1) > 0 && strlen($fecha2) == 0){
                Yii::$app->session->setFlash(
                    'danger','¡Error! Debe ingresar la Fecha Fin'
                );
                return $this->render('servicioPorTecnico', [
                    'model' => $model,
                    'modelTiposervicio' => $modelTiposervicio,
                ]);
            }else{
                if(strlen($fecha1) == 0 && strlen($fecha2) > 0){
                    Yii::$app->session->setFlash(
                        'danger','¡Error! Debe ingresar la Fecha Inicio'
                    );
                    return $this->render('servicioPorTecnico', [
                        'model' => $model,
                        'modelTiposervicio' => $modelTiposervicio,
                    ]);
                }
            }
        }
        if(strlen($fecha1) > 0 && strlen($fecha2) > 0){
            return $this->render('servicioPorTecnicoFecha', [
                'model' => $model,
                'modelTiposervicio' => $modelTiposervicio,
                'fechaInicio' => $fecha1,
                'fechaFin' => $fecha2
            ]);
        }
    }




}
