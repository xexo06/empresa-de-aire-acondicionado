<?php

namespace app\controllers;

use Yii;
use app\models\Proveedor;
use app\models\ProveedorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Usuarios;


/**
 * ProveedorController implements the CRUD actions for Proveedor model.
 */
class ProveedorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'=>['GET'],
                ],
            ],
            'access' =>[
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','view','create','createajax','update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions'=>['index','view','create','createajax','update','delete'],
                        'roles' =>['@'],
                        'matchCallback'=> function ($rule, $action){
                            return Usuarios::isUserAdministrador(Yii::$app->user->identity->Username);
                        }
                    ],

                    [
                        'allow' => true,
                        'actions'=>['index','view','create','createajax','update','delete'],
                        'roles' =>['@'],
                        'matchCallback'=> function ($rule, $action){
                            return Usuarios::isUserSecretaria(Yii::$app->user->identity->Username);
                        }
                    ],

                ],
            ],
        ];
    }

    /**
     * Lists all Proveedor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProveedorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Proveedor model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Proveedor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Proveedor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->RUT]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateajax()
    {
        $model = new Proveedor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->refresh();
            Yii::$app->response->format = 'json';
            return ['message' => Yii::t('app','¡El Proveedor ha sido creado con éxito!'), 'id'=>$model->RUT];
        }else{
            return $this->renderAjax('createajax', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Proveedor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash(
                'success','¡Proveedor actualizado con éxito!'
            );
            return $this->redirect(['view', 'id' => $model->RUT]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Proveedor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        if($id==1) {
            Yii::$app->session->setFlash(
                'danger', '¡El proveedor no se puede eliminar!'
            );
            return $this->redirect(['index']);//controlador
            // render vista

        }

        $this->findModel($id)->delete();
        Yii::$app->session->setFlash(
            'success','¡Proveedor eliminado con éxito!'
        );
        return $this->redirect(['index']);
    }

    /**
     * Finds the Proveedor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Proveedor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proveedor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
