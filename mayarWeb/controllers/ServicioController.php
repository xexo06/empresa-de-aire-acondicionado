<?php

namespace app\controllers;

use app\models\Cuotas;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;

use app\models\ServicioSearch;
use app\models\Servicio;
use app\models\Equipo;
use app\models\Tecnico;
use app\models\Producto;
use app\models\Pago;
use app\models\Tener;
use app\models\Trabajar;
use app\models\Garantia;
use app\models\Cliente;
use app\models\Tiposervicio;
use app\models\ModelEquipo;
use app\models\ModelTecnico;
use app\models\ModelGarantia;
use app\models\ModelTrabajar;
use app\models\ModelTener;
use app\models\Usuarios;


/**
 * ServicioController implements the CRUD actions for Servicio model.
 */
class ServicioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [

                ],
            ],
            'access' =>[
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','view','create','update','delete','viewinstalacionventa','viewmantencion','viewrenovaciongarantia','servicioperiodico','servicioperiodicofecha','deleteinstalacionventa','createinstalacionventa','createmantencion','updateverificacion','updateinstalacionventa','updatemantencion','deletemantencion','createrenovaciongarantia','updaterenovaciongarantia','deleterenovaciongarantia','calendario','exportarpdfinstalacion','exportarpdfserviciomantencion'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions'=>['index','view','create','update','delete','viewinstalacionventa','viewmantencion','viewrenovaciongarantia','servicioperiodico','servicioperiodicofecha','deleteinstalacionventa','createinstalacionventa','createmantencion','updateverificacion','updateinstalacionventa','updatemantencion','deletemantencion','createrenovaciongarantia','updaterenovaciongarantia','deleterenovaciongarantia','calendario','exportarpdfinstalacion','exportarpdfserviciomantencion'],
                        'roles' =>['@'],
                        'matchCallback'=> function ($rule, $action){
                            return Usuarios::isUserAdministrador(Yii::$app->user->identity->Username);
                        }
                    ],

                    [
                        'allow' => true,
                        'actions'=>['index','view','create','update','delete','viewinstalacionventa','viewmantencion','viewrenovaciongarantia','deleteinstalacionventa','createinstalacionventa','createmantencion','updateverificacion','updateinstalacionventa','updatemantencion','deletemantencion','createrenovaciongarantia','updaterenovaciongarantia','deleterenovaciongarantia','calendario','exportarpdfinstalacion','exportarpdfserviciomantencion'],
                        'roles' =>['@'],
                        'matchCallback'=> function ($rule, $action){
                            return Usuarios::isUserSecretaria(Yii::$app->user->identity->Username);
                        }
                    ],

                ],
            ],
        ];
    }

    /**
     * Lists all Servicio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServicioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    /**
     * Displays a single Servicio model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionViewinstalacionventa($id)
    {
        $sql1 = "SELECT * FROM servicio s inner JOIN trabajar t inner join tecnico te WHERE  s.nFactura=t.FK_servicio and t.FK_tecnico=te.RUT and s.nFactura=".$id." ";
        $modelsTecnicos = Tecnico::findBySql($sql1)->all();
        $sql1 = "SELECT * FROM servicio s inner JOIN tener t inner join equipo e INNER JOIN cliente c WHERE  s.nFactura=t.FK_servicio and t.FK_equipo=e.nSerie and  e.FK_cliente=c.RUT and s.nFactura='".$id."'";
        $cliente = Cliente::findBySql($sql1)->one();
        $sql1 = "SELECT * FROM servicio s inner JOIN tener t inner join equipo e WHERE  s.nFactura=t.FK_servicio and t.FK_equipo=e.nSerie and s.nFactura=".$id." ";
        $modelsEquipo = Equipo::findBySql($sql1)->all();
        $sql1 = "SELECT * FROM servicio s inner JOIN pago p where s.nFactura=p.FK_servicio and s.nFactura=".$id." ";
        $modelsPago = Pago::findBySql($sql1)->one();
        $modelsCuotas = new Cuotas();
        if($modelsPago == null){
            Yii::$app->session->setFlash(
                'warning','Este servicio no tiene un pago asociado'
            );
        }else{
            $sql1 = "SELECT * FROM servicio s inner JOIN pago p INNER JOIN cuotas c where s.nFactura=p.FK_servicio and p.ID=c.FK_pago and s.nFactura=".$id." ";
            $modelsCuotas = Cuotas::findBySql($sql1)->all();
        }
        return $this->render('viewInstalacionVenta', [
            'model' => $this->findModel($id),
            'modelsPago' => $modelsPago,
            'cliente' => $cliente,
            'modelsEquipo' => $modelsEquipo,
            'modelsTecnicos' => $modelsTecnicos,
            'modelsCuotas' => $modelsCuotas,
        ]);
    }

    public function actionViewmantencion($id)
    {

        $sql1 = "SELECT * FROM servicio s inner JOIN tener t inner join equipo e INNER JOIN cliente c WHERE  s.nFactura=t.FK_servicio and t.FK_equipo=e.nSerie and  e.FK_cliente=c.RUT and s.nFactura='".$id ."'";
        $modelCliente = Cliente::findBySql($sql1)->one();
        $sql1 = "SELECT * FROM servicio s inner JOIN tener t inner join equipo e WHERE  s.nFactura=t.FK_servicio and t.FK_equipo=e.nSerie and s.nFactura=".$id." ";
        $modelsEquipo = Equipo::findBySql($sql1)->all();
        $sql1 = "SELECT * FROM servicio s inner JOIN trabajar t inner join tecnico te WHERE  s.nFactura=t.FK_servicio and t.FK_tecnico=te.RUT and s.nFactura=".$id." ";
        $modelsTecnicos = Tecnico::findBySql($sql1)->all();
        $sql1 = "SELECT * FROM servicio s inner JOIN pago p where s.nFactura=p.FK_servicio and s.nFactura=".$id." ";
        $modelsPago = Pago::findBySql($sql1)->one();
        $modelsCuotas = new Cuotas();
        if($modelsPago == null){
            Yii::$app->session->setFlash(
                'warning','Este servicio no tiene un pago asociado'
            );
        }else{
            $sql1 = "SELECT * FROM servicio s inner JOIN pago p INNER JOIN cuotas c where s.nFactura=p.FK_servicio and p.ID=c.FK_pago and s.nFactura=".$id." ";
            $modelsCuotas = Cuotas::findBySql($sql1)->all();
        }
        return $this->render('viewMantencion', [
            'model' => $this->findModel($id),
            'modelsPago' => $modelsPago,
            'modelCliente' => $modelCliente,
            'modelsEquipo' => $modelsEquipo,
            'modelsTecnicos' => $modelsTecnicos,
            'modelsCuotas' => $modelsCuotas,
        ]);
    }

    public function actionViewrenovaciongarantia($id)
    {
        $sql1 = "SELECT * FROM servicio s inner JOIN tener t inner join equipo e INNER JOIN cliente c WHERE  s.nFactura=t.FK_servicio and t.FK_equipo=e.nSerie and  e.FK_cliente=c.RUT and s.nFactura='".$id."'";
        $modelsClientes = Cliente::findBySql($sql1)->one();
        $sql1 = "SELECT * FROM servicio s inner JOIN pago p where s.nFactura=p.FK_servicio and s.nFactura=".$id." ";
        $modelsPago = Pago::findBySql($sql1)->one();
        $sql1 = "SELECT * FROM servicio s inner JOIN tener t inner join equipo e WHERE  s.nFactura=t.FK_servicio and t.FK_equipo=e.nSerie and s.nFactura='".$id."' ";
        $modelsEquipo = Equipo::findBySql($sql1)->all();
        $modelsCuotas = new Cuotas();
        if($modelsPago == null){
            Yii::$app->session->setFlash(
                'warning','Este servicio no tiene un pago asociado'
            );
        }else{
            $sql1 = "SELECT * FROM servicio s inner JOIN pago p INNER JOIN cuotas c where s.nFactura=p.FK_servicio and p.ID=c.FK_pago and s.nFactura=".$id." ";
            $modelsCuotas = Cuotas::findBySql($sql1)->all();
        }
        return $this->render('viewRenovacionGarantia', [
            'model' => $this->findModel($id),
            'modelsPago' => $modelsPago,
            'modelsCuotas' => $modelsCuotas,
            'modelsClientes' => $modelsClientes,
            'modelsEquipo' => $modelsEquipo
        ]);
    }

    public function actionServicioperiodico(){
        $modelTiposervicio = Tiposervicio::find()->all();
        return $this->render('servicioPeriodico', [
            'modelTiposervicio' => $modelTiposervicio,
        ]);
    }
    public function actionServicioperiodicofecha(){
        $fecha1 = $_GET['fecha1'];
        $fecha2 = $_GET['fecha2'];
        $modelTiposervicio = Tiposervicio::find()->all();
        if(strlen($fecha1) == 0 && strlen($fecha2) == 0){
            Yii::$app->session->setFlash(
                'danger','¡Error! Debe ingresar las fechas'
            );
            return $this->render('servicioPeriodico', [
                'modelTiposervicio' => $modelTiposervicio,
            ]);
        }else{
            if(strlen($fecha1) > 0 && strlen($fecha2) == 0){
                Yii::$app->session->setFlash(
                    'danger','¡Error! Debe ingresar la Fecha Fin'
                );
                return $this->render('servicioPeriodico', [
                    'modelTiposervicio' => $modelTiposervicio,
                ]);
            }else{
                if(strlen($fecha1) == 0 && strlen($fecha2) > 0){
                    Yii::$app->session->setFlash(
                        'danger','¡Error! Debe ingresar la Fecha Inicio'
                    );
                    return $this->render('servicioPeriodico', [
                        'modelTiposervicio' => $modelTiposervicio,
                    ]);
                }
            }
        }
        if(strlen($fecha1) > 0 && strlen($fecha2) > 0){
            return $this->render('servicioPeriodicoFecha', [
                'modelTiposervicio' => $modelTiposervicio,
                'fechaInicio' => $fecha1,
                'fechaFin' => $fecha2
            ]);
        }
    }
    /**
     * Creates a new Servicio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Servicio();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->nFactura]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Servicio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->nFactura]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);




    }

    /**
     * Deletes an existing Servicio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteinstalacionventa($id)
    {

        $model = $this->findModel($id);

        $sql1 = "SELECT * FROM equipo e INNER JOIN tener t WHERE e.nSerie = t.FK_equipo and t.FK_servicio=".$id." ";
        $modelsEquipo = Equipo::findBySql($sql1)->all();

        $sql3 = "SELECT * FROM equipo e INNER JOIN tener t WHERE e.nSerie = t.FK_equipo and t.FK_servicio=".$id." ";
        $modelsTener = Tener::findBySql($sql3)->all();

        $sql4 = "SELECT * FROM trabajar ta INNER JOIN tecnico te WHERE ta.FK_tecnico = te.RUT and ta.FK_servicio=".$id." ";
        $modelsTrabajar= Trabajar::findBySql($sql4)->all();


        $sql5 = "SELECT * FROM equipo e INNER JOIN tener t INNER JOIN garantia g WHERE e.nSerie = t.FK_equipo and t.FK_equipo=g.FK_equipo and t.FK_servicio=".$id." ";
        $modelsGarantia=Garantia::findBySql($sql5)->all();

        foreach($modelsEquipo as $e){
            $modelProducto = Producto::findOne($e->FK_producto);
            $modelProducto->stock= $modelProducto->stock + 1;
            $modelProducto->save();
        }

        $oldIDs1 = ArrayHelper::map($modelsEquipo, 'nSerie', 'nSerie');
        $modelsEquipo= ModelEquipo::createMultiple(Equipo::classname(), $modelsEquipo);
        ModelEquipo::loadMultiple($modelsEquipo, Yii::$app->request->post());
        $deletedIDs1 = array_diff($oldIDs1, array_filter(ArrayHelper::map($modelsEquipo, 'nSerie', 'nSerie')));

        $oldIDs3 = ArrayHelper::map($modelsTener , 'ID', 'ID');
        $modelsTener = ModelTener::createMultiple(Tener::classname(), $modelsTener );
        ModelTener::loadMultiple($modelsTener, Yii::$app->request->post());
        $deletedIDs3 = array_diff($oldIDs3, array_filter(ArrayHelper::map($modelsTener, 'ID', 'ID')));

        $oldIDs4 = ArrayHelper::map($modelsTrabajar , 'ID', 'ID');
        $modelsTrabajar = ModelTrabajar::createMultiple(Trabajar::classname(), $modelsTrabajar );
        ModelTrabajar::loadMultiple($modelsTrabajar, Yii::$app->request->post());
        $deletedIDs4 = array_diff($oldIDs4, array_filter(ArrayHelper::map($modelsTrabajar , 'ID', 'ID')));

        $oldIDs5 = ArrayHelper::map($modelsGarantia , 'nGarantia', 'nGarantia');
        $modelsGarantia = ModelGarantia::createMultiple(Garantia::classname(), $modelsGarantia );
        ModelGarantia::loadMultiple($modelsGarantia, Yii::$app->request->post());
        $deletedIDs5 = array_diff($oldIDs5, array_filter(ArrayHelper::map($modelsGarantia , 'nGarantia', 'nGarantia')));





        Tener::deleteAll(['ID' => $deletedIDs3]);
        Garantia::deleteAll(['nGarantia' => $deletedIDs5]);
        Equipo::deleteAll(['nSerie' => $deletedIDs1]);
        Trabajar::deleteAll(['ID' => $deletedIDs4]);
        $this->findModel($id)->delete();



        Yii::$app->session->setFlash(
            'success','Servicio eliminado correctamente'
        );
        return $this->redirect(['createinstalacionventa']);
    }
    /**
     * Finds the Servicio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Servicio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Servicio::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionCreateinstalacionventa()
    {
        $searchModel = new ServicioSearch();
        $instalacion = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$instalacion);
        $bloquearNFactura = false;

        $model = new Servicio();
        $modelsEquipo = [new Equipo];
        $modelsTecnico = [new Tecnico];
        $cliente = new Cliente();
        if ($model->load(Yii::$app->request->post())) {



            $modelsEquipo = ModelEquipo::createMultiple(Equipo::classname());
            ModelEquipo::loadMultiple($modelsEquipo, Yii::$app->request->post());

            $modelsTecnico = ModelTecnico::createMultiple(Tecnico::classname());
            ModelTecnico::loadMultiple($modelsTecnico, Yii::$app->request->post());

            // validate all models

            $valid = $model ->validate();
            $valid = ModelEquipo::validateMultiple($modelsEquipo) && $valid;
            $cliente->load(Yii::$app->request->post());
            if($model->nGarantia == ""){
                $valid = false;
                Yii::$app->session->setFlash(
                    'danger','¡Error! Debe ingresar un N° Cert. de Garantía'
                );
            }else{
                $consulta = "select * from servicio where nGarantia = ".$model['nGarantia'];
                $nGarantia = Servicio::findBySql($consulta)->one();
                if($nGarantia != null){
                    Yii::$app->session->setFlash(
                        'danger','¡Error! El N° Cert. de Garantía ya está en uso'
                    );
                    $valid = false;
                }
            }
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->estado="pendiente";
                    $model->nServicioMantencion = 0;
                    $model->detalle = " ";
                    if ($flag = $model->save(false)) {

                        foreach ($modelsEquipo as $modelEquipo) {
                            $modelGarantia = new Garantia();
                            $modelEquipo->lat="0";
                            $modelEquipo->lng="0";
                            $modelEquipo->FK_cliente=$cliente->RUT;
                            if (!$flag = $modelEquipo->save(false)) {
                                $transaction->rollBack();
                                Yii::$app->session->setFlash(
                                    'danger','¡Error al crear el/los equipo(s)!'
                                );
                                break;
                            } else{
                                $modelsTener = new Tener();
                                $modelsTener->FK_servicio = $model->nFactura;
                                $modelsTener->FK_equipo = $modelEquipo->nSerie;
                                if(!$flag = $modelsTener->save(false)){
                                    $transaction->rollBack();
                                    Yii::$app->session->setFlash(
                                        'danger','¡Error al enlazar el/los equipos con el servicio!'
                                    );
                                    break;
                                }else{
                                    $modelProducto = Producto::findOne($modelEquipo->FK_producto);
                                    $modelProducto->stock= $modelProducto->stock - 1;
                                    if($modelProducto->stock < 1){
                                        $transaction->rollBack();
                                        Yii::$app->session->setFlash(
                                            'danger','¡Error no hay stock!'
                                        );
                                        break;
                                    }else{
                                        if(!$flag = $modelProducto->save(false)){
                                            $transaction->rollBack();
                                            Yii::$app->session->setFlash(
                                                'danger','¡Error al disminuir el stock del producto!'
                                            );
                                            break;
                                        }else{
                                            $modelGarantia->FK_equipo = $modelEquipo->nSerie;
                                            $modelGarantia->estado = "si";
                                            $nuevafecha = strtotime('+1 year', strtotime($model->fecha));
                                            $nuevafecha = date('Y-m-d', $nuevafecha);
                                            $modelGarantia->fechaFin = $nuevafecha;
                                            if(!$flag = $modelGarantia->save(false)){
                                                $transaction->rollBack();
                                                Yii::$app->session->setFlash(
                                                    'danger','¡Error al asignar la Garantía al equipo!'
                                                );
                                                break;
                                            }
                                        }
                                    }

                                }
                            }

                        }
                        foreach($modelsTecnico as $modelTecnico ){
                            $modelsTrabajar = new Trabajar();
                            $modelsTrabajar->FK_tecnico= $modelTecnico->RUT;
                            $modelsTrabajar->FK_servicio =$model->nFactura;
                            if(!$flag = $modelsTrabajar->save(false)){
                                $transaction->rollBack();
                                Yii::$app->session->setFlash(
                                    'danger','¡Error al enlazar el/los técnicos con el servicio!'
                                );
                                break;
                            }
                        }
                        if ($flag) {
                            $transaction->commit();
                            Yii::$app->session->setFlash(
                                'success','Servicio creado correctamente'
                            );
                            return $this->redirect(['viewinstalacionventa', 'id' => $model->nFactura]);
                        }
                    }
                } catch (Exception $e) {
                    Yii::$app->session->setFlash(
                        'danger','¡Error, el servicio no fue creado!'
                    );
                    $transaction->rollBack();
                }
            }

        }

        return $this->render('createInstalacionVenta', [
            'model' => $model,
            'cliente' => $cliente,
            'bloquearNFactura' => $bloquearNFactura,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelsEquipo' => (empty($modelsEquipo)) ? [new Equipo] : $modelsEquipo,
            'modelsTecnico' => (empty($modelsTecnico)) ? [new Tecnico] : $modelsTecnico,
        ]);
    }

    public function actionCreatemantencion()
    {
        $searchModel = new ServicioSearch();
        $mantencion = 3;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$mantencion);
        $model = new Servicio();
        $modelsEquipo= [new Equipo];
        $modelsTecnico = [new Tecnico];
        $bloquearNFactura = false;
        //actualizacion de base de datos garantias: Con esto se actualizaran las ultimas garantias que sean la tercera de
        // una secuancia de repeticiones cambiando el estado de "si" a "no";




        if ($model->load(Yii::$app->request->post())) {
            $model->estado="pendiente";
            $model->FK_tipoServicio=3;

            $modelsEquipo = ModelEquipo::createMultiple(Equipo::classname());
            ModelEquipo::loadMultiple($modelsEquipo, Yii::$app->request->post());

            $modelsTecnico = ModelTecnico::createMultiple(Tecnico::classname());
            ModelTecnico::loadMultiple($modelsTecnico, Yii::$app->request->post());


            // validate all models
            $valid = $model ->validate();
            /* $valid = ModelEquipo::validateMultiple($modelsEquipo) && $valid;*/
            if($model->nServicioMantencion == ""){
                $valid = false;
                Yii::$app->session->setFlash(
                    'danger','¡Error! Debe ingresar un N° Informe Servicio y Mantención'
                );
            }else{
                $consulta = "select * from servicio where nServicioMantencion = ".$model['nServicioMantencion'];
                $nSM = Servicio::findBySql($consulta)->one();
                if($nSM != null){
                    Yii::$app->session->setFlash(
                        'danger','¡Error!El N° Informe Servicio y Mantención ya está en uso'
                    );
                    $valid = false;
                }
            }
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelsEquipo as $modelEquipo) {

                            $modelsTener = new Tener();
                            $modelsTener->FK_servicio = $model->nFactura;
                            $modelsTener->FK_equipo = $modelEquipo->nSerie;
                            $modelsTener->save();


                        }
                    }

                    foreach($modelsTecnico as $modelTecnico ){

                        $modelsTrabajar = new Trabajar();
                        $modelsTrabajar->FK_tecnico= $modelTecnico->RUT;
                        $modelsTrabajar->FK_servicio =$model->nFactura;
                        $modelsTrabajar->save();
                    }

                    if ($flag) {
                        $transaction->commit();

                        return $this->redirect(['viewmantencion', 'id' => $model->nFactura]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }

        }

        return $this->render('createMantencion', [
            'model' => $model,
            'modelsEquipo'=> (empty($modelsEquipo)) ? [new Equipo] : $modelsEquipo,
            'modelsTecnico'=>(empty($modelsTecnico)) ? [new Tecnico] : $modelsTecnico,
            'bloquearNFactura' => $bloquearNFactura,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdateverificacion($id)
    {
        $direccion = $_GET['servicio'];
        $servicio = Servicio::find()->where(['nFactura' => $id])->one();
        if($servicio!=null) {
            if ($servicio->FK_tipoServicio == 1 || $servicio->FK_tipoServicio == 2) {
                return $this->redirect(['updateinstalacionventa', 'id' => $id]);
            } else {
                if ($servicio->FK_tipoServicio == 3) {
                    return $this->redirect(['updatemantencion', 'id' => $id]);
                } else {
                    return $this->redirect(['updaterenovaciongarantia', 'id' => $id]);
                }
            }
        }else{
            Yii::$app->session->setFlash(
                'danger','¡Error! La factura no existe'
            );
            return $this->redirect('index.php?r=servicio%2Fcreate'.$direccion);
        }
    }

    public function actionUpdateinstalacionventa($id)
    {

        $bloquearNFactura = true;
        $model = $this->findModel($id);

        $sql1 = "SELECT * FROM equipo e join tener t on e.nSerie = t.FK_equipo WHERE t.FK_servicio=".$id." ";
        $modelsEquipo = Equipo::findBySql($sql1)->all();
        $sql2 = "SELECT * FROM trabajar ta INNER JOIN tecnico te WHERE ta.FK_tecnico = te.RUT and ta.FK_servicio=".$id." ";
        $modelsTecnico = Tecnico::findBySql($sql2)->all();
        $sql3 = "SELECT * FROM equipo e INNER JOIN tener t WHERE e.nSerie = t.FK_equipo and t.FK_servicio=".$id." ";
        $modelsTener = Tener::findBySql($sql3)->all();
        $sql4 = "SELECT * FROM trabajar ta INNER JOIN tecnico te WHERE ta.FK_tecnico = te.RUT and ta.FK_servicio=".$id." ";
        $modelsTrabajar= Trabajar::findBySql($sql4)->all();
        $sql5 = "SELECT * FROM equipo e INNER JOIN tener t INNER JOIN garantia g WHERE e.nSerie = t.FK_equipo and t.FK_equipo=g.FK_equipo and t.FK_servicio=".$id." ";
        $modelsGarantia=Garantia::findBySql($sql5)->all();
        $sql6 = "SELECT * FROM cliente where RUT IN (SELECT e.FK_cliente FROM equipo e join tener t on e.nSerie = t.FK_equipo WHERE t.FK_servicio=".$id." )";
        $cliente = Cliente::findBySql($sql6)->one();
        $sql1 = "SELECT * FROM servicio s inner JOIN pago p where s.nFactura=p.FK_servicio and s.nFactura=".$id." ";
        $modelsPago = Pago::findBySql($sql1)->one();

        if ($model->load(Yii::$app->request->post())) {

            $oldIDs1 = ArrayHelper::map($modelsEquipo, 'nSerie', 'nSerie');
            $modelsEquipo= ModelEquipo::createMultiple(Equipo::classname(), $modelsEquipo);
            ModelEquipo::loadMultiple($modelsEquipo, Yii::$app->request->post());
            $deletedIDs1 = array_diff($oldIDs1, array_filter(ArrayHelper::map($modelsEquipo, 'nSerie', 'nSerie')));

            $oldIDs2= ArrayHelper::map($modelsTecnico , 'RUT', 'RUT');
            $modelsTecnico = ModelTecnico::createMultiple(Tecnico::classname(), $modelsTecnico );
            ModelTecnico::loadMultiple($modelsTecnico, Yii::$app->request->post());
            $deletedIDs2 = array_diff($oldIDs2, array_filter(ArrayHelper::map($modelsTecnico , 'RUT', 'RUT')));

            $oldIDs3 = ArrayHelper::map($modelsTener , 'ID', 'ID');
            $modelsTener = ModelTener::createMultiple(Tener::classname(), $modelsTener );
            ModelTener::loadMultiple($modelsTener, Yii::$app->request->post());
            $deletedIDs3 = array_diff($oldIDs3, array_filter(ArrayHelper::map($modelsTener, 'ID', 'ID')));

            $oldIDs4 = ArrayHelper::map($modelsTrabajar , 'ID', 'ID');
            $modelsTrabajar = ModelTrabajar::createMultiple(Trabajar::classname(), $modelsTrabajar );
            ModelTrabajar::loadMultiple($modelsTrabajar, Yii::$app->request->post());
            $deletedIDs4 = array_diff($oldIDs4, array_filter(ArrayHelper::map($modelsTrabajar , 'ID', 'ID')));

            $oldIDs5 = ArrayHelper::map($modelsGarantia , 'nGarantia', 'nGarantia');
            $modelsGarantia = ModelGarantia::createMultiple(Garantia::classname(), $modelsGarantia );
            ModelGarantia::loadMultiple($modelsGarantia, Yii::$app->request->post());
            $deletedIDs5 = array_diff($oldIDs5, array_filter(ArrayHelper::map($modelsGarantia , 'nGarantia', 'nGarantia')));



            /*// ajax validation
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ArrayHelper::merge(
                    ActiveForm::validateMultiple($modelsAddress),
                    ActiveForm::validate($modelCustomer)
                );
            }*/

            // validate all models
            $valid = $model ->validate();
            $valid = ModelEquipo::validateMultiple($modelsEquipo) && $valid;
            if($model->nGarantia == ""){
                $valid = false;
                Yii::$app->session->setFlash(
                    'danger','¡Error! Debe ingresar un N° Cert. de Garantía'
                );
            }else{
                $consulta = "select * from servicio where nGarantia = ".$model['nGarantia'];
                $nGarantia = Servicio::findBySql($consulta)->one();
                if($nGarantia != null){
                    if($model->nFactura != $nGarantia->nFactura){
                        Yii::$app->session->setFlash(
                            'danger','¡Error! El N° Cert. de Garantía ya está en uso'
                        );
                        $valid = false;
                    }

                }
            }

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs3)) {
                            Tener::deleteAll(['ID' => $deletedIDs3]);
                        }
                        if (! empty($deletedIDs5)) {
                            Garantia::deleteAll(['nGarantia' => $deletedIDs5]);
                        }

                        if (! empty($deletedIDs1)) {
                            foreach($deletedIDs1 as $e){

                                $equipo = Equipo::findOne($e);
                                $modelProducto = Producto::findOne($equipo->FK_producto);
                                $modelProducto->stock= $modelProducto->stock + 1;
                                $modelProducto->save();
                            }
                            Equipo::deleteAll(['nSerie' => $deletedIDs1]);
                        }
                        if (! empty($deletedIDs4)) {
                            Trabajar::deleteAll(['ID' => $deletedIDs4]);
                        }
                    }
                    $cliente->load(Yii::$app->request->post());
                    foreach ($modelsEquipo as $modelEquipo) {
                        $modelGarantia = new Garantia();
                        $modelEquipo->lat="0";
                        $modelEquipo->lng="0";


                        $equipo = Equipo::findOne($modelEquipo);
                        if($equipo!=null) {
                            $modelProducto = Producto::findOne($equipo->FK_producto);
                            $modelProducto->stock = $modelProducto->stock + 1;
                            $modelProducto->save();
                        }

                        $modelEquipo->FK_cliente=$cliente->RUT;

                        if (!($flag = $modelEquipo->save(false))) {
                            $transaction->rollBack();
                            break;
                        } else{
                            $modelsTener = new Tener();
                            $modelsTener->FK_servicio = $model->nFactura;
                            $modelsTener->FK_equipo = $modelEquipo->nSerie;
                            $modelsTener->save();


                            $modelProducto = Producto::findOne($modelEquipo->FK_producto );
                            $modelProducto->stock= $modelProducto->stock - 1;


                            $modelGarantia->FK_equipo = $modelEquipo->nSerie;
                            $modelGarantia->estado = "si";
                            $nuevafecha = strtotime('+1 year', strtotime($model->fecha));
                            $nuevafecha = date('Y-m-d', $nuevafecha);
                            $modelGarantia->fechaFin = $nuevafecha;
                            $modelGarantia->save();

                            if($modelProducto->stock < 1){
                                $transaction->rollBack();
                                break;
                            }
                            $modelProducto->save();
                        }
                    }
                    foreach($modelsTecnico as $modelTecnico ){
                        $modelsTrabajar = new Trabajar();
                        $modelsTrabajar->FK_tecnico= $modelTecnico->RUT;
                        $modelsTrabajar->FK_servicio =$model->nFactura;
                        $modelsTrabajar->save();
                    }
                    if ($flag) {
                        $transaction->commit();
                        foreach ($modelsEquipo as $modelEquipo) {
                            Yii::$app->session->setFlash(
                                'success', 'Servicio modificado correctamente'
                            );
                        }
                        return $this->redirect(['viewinstalacionventa', 'id' => $id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }

        }


        return $this->render('updateInstalacionVenta', [
            'model' => $model,
            'bloquearNFactura' => $bloquearNFactura,
            'modelsPago' => $modelsPago,
            'modelsEquipo' => (empty($modelsEquipo)) ? [new Equipo] : $modelsEquipo,
            'modelsTecnico' => (empty($modelsTecnico)) ? [new Tecnico] : $modelsTecnico,
            'cliente' => $cliente,
        ]);

    }

    public function actionUpdatemantencion($id){
        $bloquearNFactura = true;
        $model = $this->findModel($id);

        $sql1 = "SELECT * FROM equipo e INNER JOIN tener t WHERE e.nSerie = t.FK_equipo and t.FK_servicio=".$id." ";
        $modelsEquipo = Equipo::findBySql($sql1)->all();

        $sql2 = "SELECT * FROM trabajar ta INNER JOIN tecnico te WHERE ta.FK_tecnico = te.RUT and ta.FK_servicio=".$id." ";
        $modelsTecnico = Tecnico::findBySql($sql2)->all();

        $sql3 = "SELECT * FROM equipo e INNER JOIN tener t WHERE e.nSerie = t.FK_equipo and t.FK_servicio=".$id." ";
        $modelsTener = Tener::findBySql($sql3)->all();

        $sql4 = "SELECT * FROM trabajar ta INNER JOIN tecnico te WHERE ta.FK_tecnico = te.RUT and ta.FK_servicio=".$id." ";
        $modelsTrabajar= Trabajar::findBySql($sql4)->all();

        if ($model->load(Yii::$app->request->post())) {

            $oldIDs1 = ArrayHelper::map($modelsEquipo, 'nSerie', 'nSerie');
            $modelsEquipo= ModelEquipo::createMultiple(Equipo::classname(), $modelsEquipo);
            ModelEquipo::loadMultiple($modelsEquipo, Yii::$app->request->post());
            $deletedIDs1 = array_diff($oldIDs1, array_filter(ArrayHelper::map($modelsEquipo, 'nSerie', 'nSerie')));

            $oldIDs2= ArrayHelper::map($modelsTecnico , 'RUT', 'RUT');
            $modelsTecnico = ModelTecnico::createMultiple(Tecnico::classname(), $modelsTecnico );
            ModelTecnico::loadMultiple($modelsTecnico, Yii::$app->request->post());
            $deletedIDs2 = array_diff($oldIDs2, array_filter(ArrayHelper::map($modelsTecnico , 'RUT', 'RUT')));

            $oldIDs3 = ArrayHelper::map($modelsTener , 'ID', 'ID');
            $modelsTener = ModelTener::createMultiple(Tener::classname(), $modelsTener );
            ModelTener::loadMultiple($modelsTener, Yii::$app->request->post());
            $deletedIDs3 = array_diff($oldIDs3, array_filter(ArrayHelper::map($modelsTener, 'ID', 'ID')));

            $oldIDs4 = ArrayHelper::map($modelsTrabajar , 'ID', 'ID');
            $modelsTrabajar = ModelTrabajar::createMultiple(Trabajar::classname(), $modelsTrabajar );
            ModelTrabajar::loadMultiple($modelsTrabajar, Yii::$app->request->post());
            $deletedIDs4 = array_diff($oldIDs4, array_filter(ArrayHelper::map($modelsTrabajar , 'ID', 'ID')));

            // validate all models
            $valid = $model ->validate();
            if($model->nServicioMantencion == ""){
                $valid = false;
                Yii::$app->session->setFlash(
                    'danger','¡Error! Debe ingresar un N° Informe Servicio y Mantención'
                );
            }else{
                $consulta = "select * from servicio where nServicioMantencion = ".$model['nServicioMantencion']." or nServicioMAntencion = ".$model->nServicioMantencion;
                $nSM = Servicio::findBySql($consulta)->one();
                if($nSM != null){
                    if($model->nFactura != $nSM->nFactura){
                        Yii::$app->session->setFlash(
                            'danger','¡Error!El N° Informe Servicio y Mantención ya está en uso'
                        );
                        $valid = false;
                    }

                }
            }
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs3)) {
                            Tener::deleteAll(['ID' => $deletedIDs3]);
                        }
                        if (! empty($deletedIDs4)) {
                            Trabajar::deleteAll(['ID' => $deletedIDs4]);
                        }
                    }
                    foreach ($modelsEquipo as $modelEquipo) {
                        $modelsTener = new Tener();
                        $modelsTener->FK_servicio = $model->nFactura;
                        $modelsTener->FK_equipo = $modelEquipo->nSerie;
                        $modelsTener->save();
                    }
                    foreach($modelsTecnico as $modelTecnico ){
                        $modelsTrabajar = new Trabajar();
                        $modelsTrabajar->FK_tecnico= $modelTecnico->RUT;
                        $modelsTrabajar->FK_servicio =$model->nFactura;
                        $modelsTrabajar->save();
                    }
                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash(
                            'success','Servicio de Mantención modificado correctamente'
                        );
                        return $this->redirect(['viewmantencion', 'id' => $model->nFactura]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }else{
                Yii::$app->session->setFlash(
                    'danger','¡Error! no se ha podido actualizar los datos'
                );
            }
        }
        return $this->render('updateMantencion', [
            'model' => $model,
            'modelsEquipo' => (empty($modelsEquipo)) ? [new Equipo] : $modelsEquipo,
            'modelsTecnico' => (empty($modelsTecnico)) ? [new Tecnico] : $modelsTecnico,
            'bloquearNFactura' => $bloquearNFactura,
        ]);
    }

    public function actionDeletemantencion($id){
        $model = $this->findModel($id);

        $sql3 = "SELECT * FROM equipo e INNER JOIN tener t WHERE e.nSerie = t.FK_equipo and t.FK_servicio=".$id." ";
        $modelsTener = Tener::findBySql($sql3)->all();

        $sql4 = "SELECT * FROM trabajar ta INNER JOIN tecnico te WHERE ta.FK_tecnico = te.RUT and ta.FK_servicio=".$id." ";
        $modelsTrabajar= Trabajar::findBySql($sql4)->all();

        $oldIDs3 = ArrayHelper::map($modelsTener , 'ID', 'ID');
        $modelsTener = ModelTener::createMultiple(Tener::classname(), $modelsTener );
        ModelTener::loadMultiple($modelsTener, Yii::$app->request->post());
        $deletedIDs3 = array_diff($oldIDs3, array_filter(ArrayHelper::map($modelsTener, 'ID', 'ID')));

        $oldIDs4 = ArrayHelper::map($modelsTrabajar , 'ID', 'ID');
        $modelsTrabajar = ModelTrabajar::createMultiple(Trabajar::classname(), $modelsTrabajar );
        ModelTrabajar::loadMultiple($modelsTrabajar, Yii::$app->request->post());
        $deletedIDs4 = array_diff($oldIDs4, array_filter(ArrayHelper::map($modelsTrabajar , 'ID', 'ID')));

        Tener::deleteAll(['ID' => $deletedIDs3]);
        Trabajar::deleteAll(['ID' => $deletedIDs4]);
        $this->findModel($id)->delete();

        Yii::$app->session->setFlash(
            'success','Servicio de Mantención eliminado correctamente'
        );
        return $this->redirect(['createmantencion']);
    }

    public function actionCreaterenovaciongarantia(){
        $bloquearNFactura = false;
        $model = new Servicio();
        $fechaActual = date('Y-m-d');
        $sql5 = "SELECT * FROM garantia g INNER JOIN equipo e WHERE g.FK_equipo=e.nSerie and g.estado='si' and g.fechaFin<='" . $fechaActual . "' ";
        $Equipo = Equipo::findBySql($sql5)->all();
        $model->estado="realizado";
        $model->FK_tipoServicio=4;
        $searchModel = new ServicioSearch();
        $garantia = 4;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $garantia);
        //actualizacion de base de datos garantias: Con esto se actualizaran las ultimas garantias que sean la tercera de
        // una secuancia de repeticiones cambiando el estado de "si" a "no";

        $actualizar= Equipo::find()->all();
        foreach($actualizar as $actualizar1) {
            $fechaActual = date('Y-m-d');
            $sql = "SELECT * FROM garantia g WHERE g.FK_equipo=".$actualizar1['nSerie']." ";
            $count = count(Garantia::findBySql($sql)->all());
            $sql1 = "SELECT * FROM garantia g WHERE g.FK_equipo=".$actualizar1['nSerie']." and estado='si' and fechaFin<='". $fechaActual ."'";
            $siono = Garantia::findBySql($sql1)->one();
            if ($count == 3 && $siono != null) {
                $sql5 = "SELECT * FROM garantia g WHERE FK_equipo=".$actualizar1['nSerie']." and g.estado='si' and g.fechaFin<='".$fechaActual."' ";
                $Garantia = Garantia::findBySql($sql5)->one();
                $id = $Garantia['nGarantia'];
                $Garantia1 = Garantia::findOne($id);
                $estado = 'no';
                $Garantia1->estado = $estado;
                $Garantia1->update();
            }
        }


        if ($model->load(Yii::$app->request->post())) {
            $valid = true;
            $modelsEquipo = ModelEquipo::createMultiple(Equipo::classname());
            ModelEquipo::loadMultiple($modelsEquipo, Yii::$app->request->post());
            $valid = $model ->validate();

            if($model->nServicioMantencion == ""){
                $valid = false;
                Yii::$app->session->setFlash(
                    'danger','¡Error! Debe ingresar un N° Informe Servicio y Mantención'
                );
            }else{
                $consulta = "select * from servicio where nServicioMantencion = ".$model['nServicioMantencion'];
                $nSM = Servicio::findBySql($consulta)->one();
                if($nSM != null){
                    Yii::$app->session->setFlash(
                        'danger','¡Error!El N° Informe Servicio y Mantención ya está en uso'
                    );
                    $valid = false;
                }
            }

            if($valid){



                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    $model->nGarantia = 0;
                    $model->detalle = "Renovar Garantia";
                    if ($flag = $model->save(false)) {
                        foreach ($modelsEquipo as $modelEquipo) {
                            $fechaActual = date('Y-m-d');
                            $sql5 = "SELECT * FROM garantia g WHERE FK_equipo=" . $modelEquipo->nSerie . " and g.estado='si' and g.fechaFin<='" . $fechaActual . "' ";
                            $Garantia = Garantia::findBySql($sql5)->one();
                            $count = "SELECT * FROM garantia WHERE FK_equipo=" . $modelEquipo->nSerie . " ";
                            $count = count(Garantia::findBySql($sql)->all());
                            $id = $Garantia['nGarantia'];
                            $Garantia1 = Garantia::findOne($id);
                            $estado = 'no';
                            $Garantia1->estado = $estado;
                            $Garantia1->update();
                            if ($count != 3) {
                                if (!$Garantia == null) {
                                    // "UPDATE garantia SET estado='no' WHERE FK_equipo=".$modelEquipo->nSerie." and g.estado='si'";
                                    $id = $Garantia['nGarantia'];
                                    $Garantia1 = Garantia::findOne($id);
                                    $estado = 'no';
                                    $Garantia1->estado = $estado;
                                    $Garantia1->update();

                                    $modelGarantia = new Garantia();
                                    $modelGarantia->estado = "si";
                                    $modelGarantia->FK_equipo = $modelEquipo->nSerie;
                                    $nuevafecha = strtotime('+1 year', strtotime($model->fecha));
                                    $nuevafecha = date('Y-m-d', $nuevafecha);
                                    $modelGarantia->fechaFin = $nuevafecha;

                                    $modelsTener = new Tener();
                                    $modelsTener->FK_servicio = $model->nFactura;
                                    $modelsTener->FK_equipo = $modelEquipo->nSerie;
                                    $modelsTener->save();


                                    if (!($flag = $modelGarantia->save(false))) {
                                        $transaction->rollBack();
                                        break;
                                    }

                                }
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash(
                            'success','Garantía renovada con éxito'
                        );
                        return $this->redirect(['viewrenovaciongarantia', 'id' => $model->nFactura]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }

        }

        return $this->render('createrenovaciongarantia', [
            'model' => $model,
            'equipo' => $Equipo,
            'bloquearNFactura' => $bloquearNFactura,
            'modelsEquipo'=> (empty($modelsEquipo)) ? [new Equipo] : $modelsEquipo,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdaterenovaciongarantia($id){
        $bloquearNFactura = true;
        $model = $this->findModel($id);

        $sql1 = "SELECT * FROM equipo e INNER JOIN tener t WHERE e.nSerie = t.FK_equipo and t.FK_servicio=".$id." ";
        $modelsEquipo = Equipo::findBySql($sql1)->all();

        $sql3 = "SELECT * FROM equipo e INNER JOIN tener t WHERE e.nSerie = t.FK_equipo and t.FK_servicio=".$id." ";
        $modelsTener = Tener::findBySql($sql3)->all();

        $sql5 = "SELECT * FROM equipo where nSerie in (select nSerie from equipo join garantia on nSerie = FK_equipo where estado = 'si' and fechaFin < now()) or nSerie in (select nSerie from equipo join tener on nSerie = FK_equipo where FK_servicio = '".$id."') ";
        $Equipo = Equipo::findBySql($sql5)->all();
        if ($model->load(Yii::$app->request->post())) {

            $oldIDs1 = ArrayHelper::map($modelsEquipo, 'nSerie', 'nSerie');
            $modelsEquipo= ModelEquipo::createMultiple(Equipo::classname(), $modelsEquipo);
            ModelEquipo::loadMultiple($modelsEquipo, Yii::$app->request->post());
            $deletedIDs1 = array_diff($oldIDs1, array_filter(ArrayHelper::map($modelsEquipo, 'nSerie', 'nSerie')));

            $oldIDs3 = ArrayHelper::map($modelsTener , 'ID', 'ID');
            $modelsTener = ModelTener::createMultiple(Tener::classname(), $modelsTener );
            ModelTener::loadMultiple($modelsTener, Yii::$app->request->post());
            $deletedIDs3 = array_diff($oldIDs3, array_filter(ArrayHelper::map($modelsTener, 'ID', 'ID')));

            // validate all models
            $valid = $model ->validate();

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if (! empty($deletedIDs3)) {
                            Tener::deleteAll(['ID' => $deletedIDs3]);
                        }
                        foreach ($deletedIDs1 as $e){
                            $consulta = "select * from garantia where FK_equipo = ".$e." and estado = 'si'";
                            $garantia = Garantia::findBySql($consulta)->one();
                            $garantia->delete();
                            $consulta = "select * from garantia where FK_equipo = ".$e." order by fechaFin asc";
                            $garantia = Garantia::findBySql($consulta)->one();
                            $garantia->estado = "si";
                            $garantia->save();
                        }
                        foreach ($modelsEquipo as $modelEquipo) {
                            $modelsTener = new Tener();
                            $modelsTener->FK_servicio = $model->nFactura;
                            $modelsTener->FK_equipo = $modelEquipo->nSerie;
                            $modelsTener->save();

                            $consulta = "select * from garantia where FK_equipo = ".$modelEquipo->nSerie." and estado = 'si'";
                            $garantia = Garantia::findBySql($consulta)->one();
                            $fechaActual = date('Y-m-d');
                            if($garantia->fechaFin < $fechaActual){
                                $garantia->estado = "no";
                                $garantia->save();
                                $garantia = new Garantia();
                                $nuevafecha = strtotime('+1 year', strtotime($fechaActual));
                                $nuevafecha = date('Y-m-d', $nuevafecha);
                                $garantia->estado = "si";
                                $garantia->FK_equipo = $modelEquipo->nSerie;
                                $garantia->fechaFin = $nuevafecha;
                                $garantia->save();
                            }
                        }

                    }

                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash(
                            'success','Garantía actualizada con éxito'
                        );
                        return $this->redirect(['viewrenovaciongarantia', 'id' => $model->nFactura]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('updateRenovaciongarantia', [
            'model' => $model,
            'bloquearNFactura' => $bloquearNFactura,
            'Equipo' => $Equipo,
            'modelsEquipo' => (empty($modelsEquipo)) ? [new Equipo] : $modelsEquipo,
        ]);
    }

    public function actionDeleterenovaciongarantia($id){

        $model = $this->findModel($id);



        $sql3 = "SELECT * FROM equipo e INNER JOIN tener t WHERE e.nSerie = t.FK_equipo and t.FK_servicio=".$id." ";
        $modelsTener = Tener::findBySql($sql3)->all();

        $sql1 = "SELECT * FROM equipo e INNER JOIN tener t WHERE e.nSerie = t.FK_equipo and t.FK_servicio=".$id." ";
        $modelsEquipo = Equipo::findBySql($sql1)->all();

        $oldIDs3 = ArrayHelper::map($modelsTener , 'ID', 'ID');

        foreach($modelsEquipo as $e){
            $consulta = "select * from garantia where FK_equipo = ".$e['nSerie']." and estado = 'si'";
            $garantia = Garantia::findBySql($consulta)->one();
            $garantia->delete();
            $consulta = "select * from garantia where FK_equipo = ".$e['nSerie']." order by fechaFin asc";
            $garantia = Garantia::findBySql($consulta)->one();
            $garantia->estado = "si";
            $garantia->save();
        }
        Tener::deleteAll(['ID' => $oldIDs3]);
        $this->findModel($id)->delete();

        Yii::$app->session->setFlash(
            'success','Servicio de Renovación de garantía eliminado correctamente'
        );
        return $this->redirect(['createrenovaciongarantia']);
    }

    public function actionCalendario()
    {
        $servicios = Servicio::find()->All();

        $serviciosPendientes = [];

        foreach ($servicios as $servicio){
            if($servicio->estado=="pendiente") {
                $consulta = "select * from cliente c join equipo e on c.RUT = e.FK_cliente join tener t on nSerie = FK_equipo where t.FK_servicio = '".$servicio['nFactura']."'";
                $cliente = Cliente::findBySql($consulta)->one();
                $Event = new \yii2fullcalendar\models\Event();
                $Event->id = $servicio['nFactura'];

                if($servicio->FK_tipoServicio == 1){
                    $Event->title = "Instalación";
                    $Event->url = "index.php?r=servicio%2Fviewinstalacionventa&id=".$servicio['nFactura'];
                }else{
                    if($servicio->FK_tipoServicio == 2){
                        $Event->title = "Venta";
                        $Event->url = "index.php?r=servicio%2Fviewinstalacionventa&id=".$servicio['nFactura'];

                    }else{
                        if($servicio->FK_tipoServicio == 3){
                            $Event->url = "index.php?r=servicio%2Fviewmantencion&id=".$servicio['nFactura'];
                            $Event->title = "Mantención";
                        }
                    }
                }

                $Event->start = $servicio->fecha;

                /* $Event->url = "index.php?r=reserva%2Fview&id=" . $time->cod_reserva;
                 $Event->start = date('Y-m-d\TH:i:s\Z');*/

                $serviciosPendientes[] = $Event;
            }
        }
        return $this->render('calendario', [
            'events' => $serviciosPendientes,
        ]);
    }

    public function actionExportarpdfinstalacion($id)
    {

        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('pdfInstalacion',[
            'id' => $id,
        ]);

        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
            'methods' => [
            ],

        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }
    public function actionExportarpdfserviciomantencion($id)
    {

        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('pdfserviciomantencion',[
            'id' => $id,
        ]);

        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
            // call mPDF methods on the fly
            'methods' => [
            ],

        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }
}