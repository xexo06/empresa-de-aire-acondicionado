<?php

namespace app\controllers;

use app\models\Servicio;
use Yii;
use app\models\Pago;
use app\models\PagoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\Cuotas;
use app\models\ModelCuotas;
use app\models\Usuarios;

/**
 * PagoController implements the CRUD actions for Pago model.
 */
class PagoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' =>[
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','view','create','update','delete','createpagoconservicio','createpago','updatepago','deletepago','viewinstalacionventa'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions'=>['index','view','create','update','delete','createpagoconservicio','createpago','updatepago','deletepago','viewinstalacionventa'],
                        'roles' =>['@'],
                        'matchCallback'=> function ($rule, $action){
                            return Usuarios::isUserAdministrador(Yii::$app->user->identity->Username);
                        }
                    ],

                    [
                        'allow' => true,
                        'actions'=>['index','view','create','update','delete','createpagoconservicio','createpago','updatepago','deletepago','viewinstalacionventa'],
                        'roles' =>['@'],
                        'matchCallback'=> function ($rule, $action){
                            return Usuarios::isUserSecretaria(Yii::$app->user->identity->Username);
                        }
                    ],

                ],
            ],
        ];
    }

    /**
     * Lists all Pago models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PagoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pago model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pago model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Pago();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function actionCreatepagoconservicio($id){
        $model =  new Pago();
        $consultaServicio = "select * from servicio where nFactura not in (select FK_servicio from pago)";
        $modelServicio = Servicio::findBySql($consultaServicio)->all();
        $model->FK_servicio = $id;
        $modelsCuotas = [new Cuotas];
        $searchModel = new PagoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('createPago', [
            'model' => $model,
            'modelServicio' => $modelServicio,
            'modelsCuotas' => (empty($modelsCuotas)) ? [new Cuotas] : $modelsCuotas,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreatepago()
    {
        $model = new Pago();
        $modelsCuotas = [new Cuotas];
        $consultaServicio = "select * from servicio where nFactura not in (select FK_servicio from pago)";
        $modelServicio = Servicio::findBySql($consultaServicio)->all();
        $searchModel = new PagoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($model->load(Yii::$app->request->post())) {

            $modelsCuotas = ModelCuotas::createMultiple(Cuotas::classname());
            ModelCuotas::loadMultiple($modelsCuotas, Yii::$app->request->post());



            // validate all models
            $valid = $model ->validate();


            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($modelsCuotas as $modelCuotas) {

                            $modelCuotas->FK_pago = $model->ID;
                            if (!($flag = $modelCuotas->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }
                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash(
                            'success','Pago creado correctamente'
                        );
                        return $this->redirect(['servicio/viewinstalacionventa', 'id' => $model->FK_servicio]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }

        }

        return $this->render('createPago', [
            'model' => $model,
            'modelServicio' => $modelServicio,
            'modelsCuotas' => (empty($modelsCuotas)) ? [new Cuotas] : $modelsCuotas,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Pago model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    public function actionUpdatepago($id)
    {
        $model = $this->findModel($id);

        $sql1 = "SELECT * FROM cuotas WHERE FK_pago=".$id." ";
        $modelsCuotas = Cuotas::findBySql($sql1)->all();
        $consultaServicio = "select * from servicio where nFactura not in (select FK_servicio from pago) or nFactura =".$model->FK_servicio;
        $modelServicio = Servicio::findBySql($consultaServicio)->all();
        if ($model->load(Yii::$app->request->post())) {
            $oldIDs1 = ArrayHelper::map($modelsCuotas, 'ID', 'ID');
            $modelsCuotas= ModelCuotas::createMultiple(Cuotas::classname(), $modelsCuotas);
            ModelCuotas::loadMultiple($modelsCuotas, Yii::$app->request->post());
            $deletedIDs1 = array_diff($oldIDs1, array_filter(ArrayHelper::map($modelsCuotas, 'ID', 'ID')));

            $valid = $model ->validate();
            $valid = ModelCuotas::validateMultiple($modelsCuotas) && $valid;
            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        if ($flag = $model->save(false)) {
                            if (! empty($deletedIDs3)) {
                                Cuotas::deleteAll(['ID' => $deletedIDs1]);
                            }
                        }
                    }
                    foreach ($modelsCuotas as $modelCuota) {
                        $modelCuota = new Cuotas();
                        $modelCuota->FK_pago = $model->ID;
                        $modelCuota->save();
                    }

                    if ($flag) {
                        $transaction->commit();
                        Yii::$app->session->setFlash(
                            'success','Pago actualizado con éxito'
                        );
                        return $this->redirect(['updatepago', 'id' => $model->ID]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('updatePago', [
            'model' => $model,
            'modelsCuotas' => $modelsCuotas,
            'modelServicio' => $modelServicio,
        ]);
    }

    /**
     * Deletes an existing Pago model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeletepago($id){

        $sql1 = "SELECT * FROM cuotas WHERE FK_pago=".$id." ";
        $modelsCuotas = Cuotas::findBySql($sql1)->all();

        $oldIDs1 = ArrayHelper::map($modelsCuotas, 'ID', 'ID');
        $modelsCuotas= ModelCuotas::createMultiple(Cuotas::classname(), $modelsCuotas);
        ModelCuotas::loadMultiple($modelsCuotas, Yii::$app->request->post());
        $deletedIDs1 = array_diff($oldIDs1, array_filter(ArrayHelper::map($modelsCuotas, 'ID', 'ID')));

        Cuotas::deleteAll(['ID' => $deletedIDs1]);
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash(
            'success','Pago eliminado correctamente'
        );
        return $this->redirect(['createpago']);
    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionViewinstalacionventa($id){
        $servicio = Servicio::find()->where(['nFactura' => $id])->one();

        if($servicio->FK_tipoServicio == 1 || $servicio->FK_tipoServicio == 2){
            return $this->redirect(array('servicio/viewinstalacionventa','id' => $id));
        }

        if($servicio->FK_tipoServicio == 3){
            return $this->redirect(array('servicio/viewmantencion','id' => $id));
        }

        if($servicio->FK_tipoServicio == 4){
            return $this->redirect(array('servicio/viewrenovaciongarantia','id' => $id));
        }
    }

    /**
     * Finds the Pago model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Pago the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pago::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
